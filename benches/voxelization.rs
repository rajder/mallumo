#![feature(test)]

extern crate test;

extern crate mallumo;

extern crate glutin;
extern crate image;

#[macro_use]
extern crate error_chain;

mod errors {
    error_chain!{}
}

use errors::*;

use std::os::raw::c_void;

use glutin::{Api, ContextBuilder, EventsLoop, GlProfile, GlRequest, GlWindow, WindowBuilder};
use mallumo::*;

#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    #[bench]
    fn bench_voxel_fragment_list_512(b: &mut Bencher) {
        let voxel_grid_size = 512;
        let mut width: u32 = 512;
        let mut height: u32 = 512;

        let mut events_loop = EventsLoop::new();
        let window = WindowBuilder::new()
            .with_title("Hello, world!")
            .with_dimensions(width, height);
        let context = ContextBuilder::new()
            .with_gl(GlRequest::Specific(Api::OpenGl, (4, 5)))
            .with_gl_profile(GlProfile::Core);
        let gl_window = match GlWindow::new(window, context, &events_loop) {
            Err(e) => panic!("OpenGL Window Creation Error: {}", e),
            Ok(window) => window,
        };

        // Creation of Renderer
        let mut renderer = Renderer::new(&gl_window).unwrap();
        renderer.make_current();

        renderer.mut_default_framebuffer().set_viewport(Viewport {
            x: 0,
            y: 0,
            width: width,
            height: height,
        });
        renderer.mut_default_framebuffer().set_disable(EnableOption::DepthTest);

        let mut shape_list = ShapeList::from_files(&["./obj_models/dragon.obj"]).unwrap();

        let voxel_fragment_list_module = VoxelFragmentListModule::new().unwrap();
        let mut voxel_fragment_list_512 = voxel_fragment_list_module
            .create_voxel_fragment_list_generator(9)
            .unwrap();

        b.iter(|| {
            let voxel_fragment_list = voxel_fragment_list_512.voxelize(&mut renderer, &shape_list);

            renderer.swap_buffers();
        });
    }

    #[bench]
    fn bench_voxel_fragment_list_optimized_512(b: &mut Bencher) {
        let voxel_grid_size = 512;
        let mut width: u32 = 512;
        let mut height: u32 = 512;

        let mut events_loop = EventsLoop::new();
        let window = WindowBuilder::new()
            .with_title("Hello, world!")
            .with_dimensions(width, height);
        let context = ContextBuilder::new()
            .with_gl(GlRequest::Specific(Api::OpenGl, (4, 5)))
            .with_gl_profile(GlProfile::Core);
        let gl_window = match GlWindow::new(window, context, &events_loop) {
            Err(e) => panic!("OpenGL Window Creation Error: {}", e),
            Ok(window) => window,
        };

        // Creation of Renderer
        let mut renderer = Renderer::new(&gl_window).unwrap();
        renderer.make_current();

        renderer.mut_default_framebuffer().set_viewport(Viewport {
            x: 0,
            y: 0,
            width: width,
            height: height,
        });
        renderer.mut_default_framebuffer().set_disable(EnableOption::DepthTest);

        let mut shape_list = ShapeList::from_files(&["./obj_models/dragon.obj"]).unwrap();

        let voxel_fragment_list_module = VoxelFragmentListModule::new().unwrap();
        let mut voxel_fragment_list_512 = voxel_fragment_list_module
            .create_voxel_fragment_list_generator(9)
            .unwrap();

        let buffer_size: usize = 512 * 512 * 512 * std::mem::size_of::<u32>();

        let mut voxel_positions_buffer = MutableBuffer::new_empty(buffer_size).unwrap();
        let mut voxel_albedos_buffer = MutableBuffer::new_empty(buffer_size).unwrap();
        let mut voxel_pbrs_buffer = MutableBuffer::new_empty(buffer_size).unwrap();
        let mut voxel_normals_buffer = MutableBuffer::new_empty(buffer_size).unwrap();

        b.iter(|| {
            let voxel_fragment_list = voxel_fragment_list_512.voxelize_optimized_preallocated(
                &mut renderer,
                &shape_list,
                &mut voxel_positions_buffer,
                &mut voxel_albedos_buffer,
                &mut voxel_pbrs_buffer,
                &mut voxel_normals_buffer,
            );

            renderer.swap_buffers();
        });
    }
}
