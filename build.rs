extern crate gcc;
extern crate include_dir;

use include_dir::include_dir;
use std::path::Path;

fn main() {
    gcc::Build::new()
        .file("mikktspace/mikktspace.c")
        .compile("libmikktspace.a");

    let shaders_dest_path = Path::new("./").join("shaders.rs");
    include_dir("assets/shaders")
        .as_variable("SRC")
        .to_file(shaders_dest_path)
        .unwrap();
}
