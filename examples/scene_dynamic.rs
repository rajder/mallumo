mod app_ui {
    use mallumo::conrod::*;
    use mallumo::*;
    use std;

    widget_ids! {
        struct Ids {
            canvas,
            slider_camera_angle_x,
            slider_camera_angle_y,
        }
    }

    pub struct Variables {
        pub camera_angle_x: f32,
        pub camera_angle_y: f32,
    }

    pub struct AppUi {
        ui: Ui,
        ids: self::Ids,
        image_map: image::Map<Texture2D>,
        pub variables: self::Variables,
    }

    impl AppUi {
        pub fn new(width: usize, height: usize) -> AppUi {
            let mut ui = UiBuilder::new([width as f64, height as f64]).build();

            let ids = self::Ids::new(ui.widget_id_generator());

            // Add fonts to ui
            // ...

            // The image map describing widget->image mappings
            let image_map = image::Map::<Texture2D>::new();

            AppUi {
                ui: ui,
                ids: ids,
                image_map: image_map,
                variables: Variables {
                    camera_angle_x: std::f32::consts::PI / 4.0f32,
                    camera_angle_y: std::f32::consts::PI / 4.0f32,
                },
            }
        }

        pub fn get_ui<'a>(&'a self) -> &'a Ui {
            &self.ui
        }

        pub fn get_image_map<'a>(&'a self) -> &'a image::Map<Texture2D> {
            &self.image_map
        }

        pub fn process_event(&mut self, event: glutin::Event, window: &glutin::GlWindow) {
            let input = match convert_event(event, &window) {
                None => return,
                Some(input) => input,
            };

            self.ui.handle_event(input);
        }

        pub fn update_ui(&mut self) {
            let ui = &mut self.ui.set_widgets();

            widget::Canvas::new()
                .w(200.0)
                .pad(10.0)
                .top_left_of(ui.window)
                .color(conrod::color::BLUE)
                .set(self.ids.canvas, ui);

            if let Some(camera_angle_x) = widget::Slider::new(self.variables.camera_angle_x, 0.0, std::f32::consts::PI)
                .w_h(180.0, 10.0)
                .top_left_of(self.ids.canvas)
                .rgb(1.0, 0.3, 0.6)
                .set(self.ids.slider_camera_angle_x, ui)
            {
                self.variables.camera_angle_x = camera_angle_x;
            }

            if let Some(camera_angle_y) = widget::Slider::new(self.variables.camera_angle_y, 0.0, std::f32::consts::PI)
                .w_h(180.0, 10.0)
                .top_left_of(self.ids.canvas)
                .down(30.0)
                .rgb(0.0, 0.3, 1.0)
                .set(self.ids.slider_camera_angle_y, ui)
            {
                self.variables.camera_angle_y = camera_angle_y;
            }
        }
    }
}

extern crate mallumo;
extern crate time;

#[macro_use]
extern crate error_chain;

mod errors {
    error_chain!{}
}

use errors::*;
use mallumo::cgmath::*;
use mallumo::glutin::*;
use mallumo::*;

quick_main!(run);

fn run() -> Result<()> {
    let mut app = AppBuilder::new()
        .with_title("Scene dynamic example")
        .with_multisampling(4)
        .build();
    let mut input = Input::default();

    let mut gui_renderer = GuiRenderer::new(app.width, app.height, app.gl_window.hidpi_factor() as f64)
        .chain_err(|| "Unable to create GUI renderer")?;
    let mut app_ui = app_ui::AppUi::new(app.width, app.height);

    app.renderer.set_viewport(Viewport {
        x: 0,
        y: 0,
        width: app.width,
        height: app.height,
    });
    app.renderer.set_enable(EnableOption::DepthTest);
    app.renderer.set_enable(EnableOption::CullFace);
    app.renderer.set_enable(EnableOption::Multisample);

    let quad = generate_quad(
        Point3::new(0.0, 0.0, 0.0),
        Vector3::new(1.0, 0.0, 0.0),
        Vector3::new(0.0, 0.0, 1.0),
        Vector4::new(1.0, 1.0, 1.0, 1.0),
        0.0,
        1.0,
        true,
        true,
    );
    let cube1 = generate_cube(
        Point3::new(0.0, 0.0, 0.0),
        0.2,
        Vector4::new(1.0, 1.0, 1.0, 1.0),
        0.0,
        1.0,
        true,
        true,
    );
    let cube2 = generate_cube(
        Point3::new(0.0, 0.36, 0.0),
        0.1,
        Vector4::new(1.0, 1.0, 1.0, 1.0),
        0.0,
        1.0,
        true,
        true,
    );

    let mut shape_list = ShapeList::from_shapes(
        vec![
            (&quad.0, &quad.1, quad.2, None, None, None, None, None),
            (&cube1.0, &cube1.1, cube1.2, None, None, None, None, None),
            (&cube2.0, &cube2.1, cube2.2, None, None, None, None, None),
        ],
        None,
        // Some(Unitization {
        //     box_min: Point3::new(-1.0, -1.0, -1.0),
        //     box_max: Point3::new(1.0, 1.0, 1.0),
        //     unitize_if_fits: true,
        // }),
    ).chain_err(|| "Could not create shape list")?;

    shape_list
        .update_buffers()
        .chain_err(|| "Could not update shape list buffers")?;

    let mut camera = SphereCamera::new(
        SpherePosition {
            radius: 1.5,
            y_angle: Rad(0.0),
            height_angle: Deg(45.0).into(),
        },
        Point3::new(0.0, 0.0, 0.0),
        Vector3::new(0.0, 1.0, 0.0),
        Deg(45.0).into(),
        app.width as usize,
        app.height as usize,
        0.0001,
        100.0,
    ).chain_err(|| "Could not create camera")?;

    let mut scene_renderer = SceneRenderer::new(&mut app.renderer).chain_err(|| "Could not create scene renderer")?;

    // Lights
    let sun_module = SunModule::new().chain_err(|| "Could not initialize Sun module")?;
    let mut sun = sun_module
        .create_sun(
            Vector3::new(2.0, 2.0, 2.0),
            SunPosition {
                y_angle: Rad(3.14159265 / 4.0),
                height_angle: Rad(3.14159265 / 4.0),
            },
            8129,
        )
        .chain_err(|| "Could not initialize the Sun")?;

    let mut light_group = LightGroup::new(vec![
        Light::new(4096, Point3::new(0.0, 2.0, 0.0), 10.0, Vector3::new(20.0, 20.0, 20.0))
            .chain_err(|| "Could not create light")?,
        Light::new(4096, Point3::new(0.0, 2.0, 0.0), 10.0, Vector3::new(10.0, 0.0, 10.0))
            .chain_err(|| "Could not create light")?,
    ]).chain_err(|| "Could not create light group")?;

    light_group
        .render_shadowmaps(&mut app.renderer, &[&shape_list])
        .chain_err(|| "Could not render shadowmaps")?;

    let mut previous_time = time::precise_time_ns();
    'render_loop: loop {
        let current_time = time::precise_time_ns();
        let delta_time = (current_time - previous_time) as f32 / 1000000000.0;

        // Process the events
        for app_event in app.poll_events() {
            app_ui.process_event(app_event.clone(), &app.gl_window);

            match app_event.clone() {
                Event::WindowEvent {
                    event: WindowEvent::CursorMoved { position, .. },
                    ..
                } => {
                    if position.0 > 220.0 {
                        input.process_event(&app_event);
                        camera
                            .process_event(&app_event, &input, delta_time)
                            .chain_err(|| "Could not process event in camera")?;
                    }
                }
                _ => {
                    input.process_event(&app_event);
                    camera
                        .process_event(&app_event, &input, delta_time)
                        .chain_err(|| "Could not process event in camera")?;
                }
            };

            match app_event {
                Event::WindowEvent {
                    event: WindowEvent::Resized(w, h),
                    ..
                } => {
                    app.renderer.set_viewport(Viewport {
                        x: 0,
                        y: 0,
                        width: w as usize,
                        height: h as usize,
                    });

                    gui_renderer
                        .resize(w as usize, h as usize)
                        .chain_err(|| "Coult not resize GUI")?;
                }
                Event::WindowEvent {
                    event: WindowEvent::Closed,
                    ..
                }
                | Event::WindowEvent {
                    event:
                        WindowEvent::KeyboardInput {
                            input:
                                KeyboardInput {
                                    virtual_keycode: Some(VirtualKeyCode::Escape),
                                    ..
                                },
                            ..
                        },
                    ..
                } => break 'render_loop,
                _ => (),
            };
        }

        let x = (current_time as f64 / 1_000_000_000.0f64).sin() as f32;
        let z = (current_time as f64 / 1_000_000_000.0f64).cos() as f32;

        shape_list.primitive_parameters[1].model_matrix =
            Matrix4::from_translation(Vector3::new(x / 1.5, 0.1015, z / 1.5));
        shape_list.primitive_parameters[2].model_matrix = Matrix4::from_angle_y(Rad(x * std::f32::consts::PI))
            * Matrix4::from_angle_x(Deg(45.0))
            * Matrix4::from_angle_z(Deg(45.0));

        shape_list
            .update_buffers()
            .chain_err(|| "Could not update shape list buffers")?;

        let pos_far = light_group.lights[0].light_parameter.pos_far;
        light_group.lights[0].light_parameter.pos_far = Vector4::new(-x, pos_far.y, z, pos_far.w);

        let pos_far = light_group.lights[1].light_parameter.pos_far;
        light_group.lights[1].light_parameter.pos_far = Vector4::new(-x, pos_far.y, -z, pos_far.w);
        light_group
            .update_buffer()
            .chain_err(|| "Could not update light group buffer")?;

        // light_group
        //     .render_shadowmaps(&mut app.renderer, &[&shape_list])
        //     .chain_err(|| "Could not render shadowmaps");

        sun.set_position(SunPosition {
            y_angle: Rad(app_ui.variables.camera_angle_x),
            height_angle: Rad(app_ui.variables.camera_angle_y),
        });

        sun.render_shadowmap(&mut app.renderer, &[&shape_list])
            .chain_err(|| "Could not render shadowmaps")?;

        // Render part of the loop
        app.renderer.clear_default_framebuffer(ClearBuffers::ColorDepth);

        // scene_renderer
        //     .render(&mut app.renderer, &[&shape_list], &camera, Some(&light_group), None)
        //     .chain_err(|| "Could not render the scene");
        scene_renderer
            .render(&mut app.renderer, &[&shape_list], &camera, None, Some(&sun))
            .chain_err(|| "Could not render the scene")?;

        // Update UI
        app_ui.update_ui();
        gui_renderer
            .render(&mut app.renderer, &app_ui.get_ui(), &app_ui.get_image_map())
            .chain_err(|| "Could not render GUI")?;

        camera.update_buffer().chain_err(|| "Could not update buffer")?;

        app.swap_buffers();

        previous_time = current_time;
    }

    Ok(())
}
