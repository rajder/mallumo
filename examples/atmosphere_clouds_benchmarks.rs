extern crate mallumo;

#[macro_use]
extern crate error_chain;

mod errors {
    error_chain!{}
}

use errors::*;
use mallumo::cgmath::*;
use mallumo::*;

quick_main!(run);

fn run() -> Result<()> {
    let resolutions = [(640, 480), (1280, 720), (1920, 1080)];

    let mut app = AppBuilder::new()
        .with_title("Kovacs Benchmarks")
        .with_dimensions(
            resolutions[resolutions.len() - 1].0,
            resolutions[resolutions.len() - 1].1,
        )
        .build();

    app.renderer.set_viewport(Viewport {
        x: 0,
        y: 0,
        width: app.width,
        height: app.height,
    });

    let atmosphere_module =
        AtmosphereModule::new(&mut app.renderer).chain_err(|| "Could not create atmosphere module")?;

    for _ in app.poll_events() {}

    atmosphere_create(&mut app).chain_err(|| "Could not bench atmosphere create")?;

    for _ in app.poll_events() {}

    for (width, height) in resolutions.iter() {
        sky_render(&mut app, &atmosphere_module, *width, *height).chain_err(|| "Could not bench sky render")?;

        for _ in app.poll_events() {}
    }

    clouds_texture_create(&mut app).chain_err(|| "Could not bench clouds texture create")?;

    for _ in app.poll_events() {}

    for (width, height) in resolutions.iter() {
        for d in [1, 2, 4].iter() {
            for r in [1, 2, 4].iter() {
                for i in [64, 128, 256, 512].iter() {
                    clouds_render(&mut app, &atmosphere_module, *width, *height, *d, *r, *i)
                        .chain_err(|| "Could not bench clouds render")?;

                    for _ in app.poll_events() {}
                }
            }
        }
    }

    Ok(())
}

fn sun_to_dir(y_angle: cgmath::Rad<f32>, h: cgmath::Rad<f32>) -> [f32; 3] {
    let x = Rad::sin(h) * Rad::sin(y_angle);
    let y = Rad::cos(h);
    let z = Rad::sin(h) * Rad::cos(y_angle);

    [x, y, z]
}

fn average(times: &[u64]) -> u64 {
    times.iter().sum::<u64>() / times.len() as u64
}

fn median_already_sorted(times: &[u64]) -> u64 {
    let mid = times.len() / 2;
    if times.len() % 2 == 0 {
        (times[mid] + times[mid - 1]) / 2
    } else {
        times[mid]
    }
}

fn print_statistics(mut times: Vec<u64>) {
    times.sort_unstable();

    print!("  best 5:");
    for i in 0..5 {
        print!(" {:?}", times[i]);
    }
    println!();
    println!("  average: {:?}", average(times.as_slice()));
    println!("  median: {:?}", median_already_sorted(times.as_slice()));
}

fn atmosphere_create(app: &mut App) -> Result<()> {
    let mut timer_query = TimerQuery::new();
    let runs = 500;
    let mut times = Vec::with_capacity(runs);

    for _ in 0..runs {
        timer_query.begin();
        let _atmosphere_module =
            AtmosphereModule::new(&mut app.renderer).chain_err(|| "Could not create atmosphere module")?;
        times.push(timer_query.end_ns());

        for _ in app.poll_events() {}
    }
    println!("ATMOSPHERE CREATE");
    print_statistics(times);

    Ok(())
}

fn sky_render(app: &mut App, atmosphere_module: &AtmosphereModule, width: usize, height: usize) -> Result<()> {
    app.width = width;
    app.height = height;
    app.renderer.set_viewport(Viewport {
        x: 0,
        y: 0,
        width: app.width,
        height: app.height,
    });

    let mut timer_query = TimerQuery::new();
    let runs = 500;
    let mut times = Vec::with_capacity(runs);

    let camera = SphereCamera::new(
        SpherePosition {
            radius: 1.5,
            y_angle: Rad(4.60625),
            height_angle: Rad(2.8),
        },
        Point3::new(0.0, 0.0, 0.0),
        Vector3::new(0.0, 1.0, 0.0),
        Deg(90.0).into(),
        app.width as usize,
        app.height as usize,
        0.0001,
        100.0,
    ).chain_err(|| "Could not create camera")?;

    let test_vertex = Shader::new(
        ShaderType::Vertex,
        &[include_str!("../assets/shaders/deferred_render.vert")],
    ).chain_err(|| "Failed to compile render vertex shader")?;

    let test_fragment = Shader::new(
        ShaderType::Fragment,
        &[
            include_str!("../assets/shaders/libs/version.glsl"),
            include_str!("../assets/shaders/libs/consts.glsl"),
            include_str!("../assets/shaders/atmosphere/structures.glsl"),
            include_str!("../assets/shaders/atmosphere/constants.glsl"),
            "#define MALLUMO_ATMOSPHERE_GLOBALS 0\n",
            include_str!("../assets/shaders/atmosphere/render.glsl"),
            include_str!("../assets/shaders/libs/filmic_tonemapping.glsl"),
            include_str!("../assets/shaders/atmosphere/render.frag"),
        ],
    ).chain_err(|| "Failed to compile render fragment shader")?;

    let test_pipeline = PipelineBuilder::new()
        .vertex_shader(&test_vertex)
        .fragment_shader(&test_fragment)
        .build()
        .chain_err(|| "Unable to build draw direct irradiance pipeline")?;

    let sun_dir = [sun_to_dir(Rad(1.3419969), Rad(0.72532487))];
    for _ in 0..runs {
        timer_query.begin();
        app.renderer.clear_default_framebuffer(ClearBuffers::ColorDepth);
        {
            let draw_command = DrawCommand::arrays(&test_pipeline, 0, 3)
                .uniform(&atmosphere_module.globals_buffer, 0)
                .uniform(camera.get_buffer(), 1)
                .uniform_3fv(&sun_dir, 0)
                .uniform_1f(1.0, 1)
                .uniform_1i(0, 2)
                .texture_2d(&atmosphere_module.transmittance_texture, 0)
                .texture_3d(&atmosphere_module.scattering_texture, 1)
                .texture_2d(&atmosphere_module.irradiance_texture, 2);

            app.renderer.draw(&draw_command).chain_err(|| "Could not draw texture")?;
        }
        app.swap_buffers();
        times.push(timer_query.end_ns());

        for _ in app.poll_events() {}
    }

    println!("SKY RENDER W:{:?} H:{:?}", width, height);
    print_statistics(times);

    Ok(())
}

fn clouds_texture_create(app: &mut App) -> Result<()> {
    let mut timer_query = TimerQuery::new();
    let runs = 500;
    let mut times = Vec::with_capacity(runs);

    for _ in 0..runs {
        timer_query.begin();

        let _perlin_worley_noise =
            create_pw1_w1_w2_w4_3d_noise(&mut app.renderer, 128).chain_err(|| "Could not create main noise texture")?;

        let _worley_noise =
            create_w1_w2_w4_3d_noise(&mut app.renderer, 32).chain_err(|| "Could not create detail noise texture")?;

        let _curl_noise = create_curl_2d_noise(&mut app.renderer, 128).chain_err(|| "Could not create curl noise")?;

        times.push(timer_query.end_ns());

        for _ in app.poll_events() {}
    }
    println!("CLOUDS CREATE");
    print_statistics(times);

    Ok(())
}

fn clouds_render(
    app: &mut App,
    atmosphere_module: &AtmosphereModule,
    width: usize,
    height: usize,
    downsample_factor: usize,
    reprojection_factor: usize,
    max_iterations: u32,
) -> Result<()> {
    app.width = width / downsample_factor;
    app.height = height / downsample_factor;
    app.renderer.set_viewport(Viewport {
        x: 0,
        y: 0,
        width: app.width,
        height: app.height,
    });
    app.renderer.set_disable(EnableOption::DepthTest);
    app.renderer.set_disable(EnableOption::CullFace);

    app.renderer.set_enable(EnableOption::Blend);
    app.renderer
        .set_blending_equation(BlendingEquation::Addition, BlendingEquation::Addition);
    app.renderer.set_linear_blending_factors(
        LinearBlendingFactor::SourceAlpha,
        LinearBlendingFactor::OneMinusSourceAlpha,
        LinearBlendingFactor::SourceAlpha,
        LinearBlendingFactor::OneMinusSourceAlpha,
    );

    let mut timer_query = TimerQuery::new();
    let runs = 500;
    let mut times = Vec::with_capacity(runs);

    let raymarch_vertex = Shader::new(
        ShaderType::Vertex,
        &[include_str!("../assets/shaders/deferred_render.vert")],
    ).chain_err(|| "Failed to compile raymarch vertex shader")?;
    let raymarch_fragment = Shader::new(
        ShaderType::Fragment,
        &[
            include_str!("../assets/shaders/libs/version.glsl"),
            include_str!("../assets/shaders/libs/consts.glsl"),
            include_str!("../assets/shaders/atmosphere/structures.glsl"),
            include_str!("../assets/shaders/atmosphere/constants.glsl"),
            "#define MALLUMO_ATMOSPHERE_GLOBALS 1\n",
            include_str!("../assets/shaders/atmosphere/render.glsl"),
            include_str!("../assets/shaders/libs/filmic_tonemapping.glsl"),
            include_str!("../assets/shaders/clouds/render.frag"),
        ],
    ).chain_err(|| "Failed to compile raymarch fragment shader")?;

    let raymarch_pipeline = PipelineBuilder::new()
        .vertex_shader(&raymarch_vertex)
        .fragment_shader(&raymarch_fragment)
        .build()
        .chain_err(|| "Unable to build raymarch pipeline")?;

    let sub_width = app.width / reprojection_factor;
    let sub_height = app.height / reprojection_factor;
    let mut reprojection_module =
        ReprojectionModule::new(Texture2DSize(sub_width, sub_height), reprojection_factor, true)
            .chain_err(|| "Could not create reproject module")?;

    let mut clouds_texture = Texture2D::new_empty(
        Texture2DSize(sub_width, sub_height),
        TextureInternalFormat::RGBA32F,
        TextureFormat::RGBA,
        TextureDataType::Byte,
        Texture2DParameters {
            min: TextureTexelFilter::Linear,
            mag: TextureTexelFilter::Linear,
            mipmap: TextureMipmapFilter::None,
            wrap_s: TextureWrapMode::ClampToEdge,
            wrap_t: TextureWrapMode::ClampToEdge,
        },
        1,
    ).chain_err(|| "Could not create sub texture")?;

    let perlin_worley_noise =
        create_pw1_w1_w2_w4_3d_noise(&mut app.renderer, 128).chain_err(|| "Could not create main noise texture")?;

    let worley_noise =
        create_w1_w2_w4_3d_noise(&mut app.renderer, 32).chain_err(|| "Could not create detail noise texture")?;

    let curl_noise = create_curl_2d_noise(&mut app.renderer, 128).chain_err(|| "Could not create curl noise")?;

    let coverage_texture = Texture2D::new(
        Texture2DSize(512, 512),
        TextureInternalFormat::RGB8,
        TextureFormat::RGB,
        TextureDataType::UnsignedByte,
        Texture2DParameters {
            min: TextureTexelFilter::Linear,
            mag: TextureTexelFilter::Linear,
            mipmap: TextureMipmapFilter::None,
            wrap_s: TextureWrapMode::Repeat,
            wrap_t: TextureWrapMode::Repeat,
        },
        1,
        include_bytes!("../assets/clouds/coverage_512_512_3.bytes"),
    ).chain_err(|| "Could not create coverage texture")?;

    let mut clouds_fbo = GeneralFramebuffer::new();
    clouds_fbo.set_viewport(Viewport {
        x: 0,
        y: 0,
        width: sub_width,
        height: sub_height,
    });
    clouds_fbo.set_disable(EnableOption::DepthTest);
    clouds_fbo.set_disable(EnableOption::CullFace);

    let mut full_frame_fbo = GeneralFramebuffer::new();
    full_frame_fbo.set_viewport(Viewport {
        x: 0,
        y: 0,
        width: app.width,
        height: app.height,
    });
    full_frame_fbo.set_disable(EnableOption::DepthTest);
    full_frame_fbo.set_disable(EnableOption::CullFace);

    let draw_texture_vertex = Shader::new(
        ShaderType::Vertex,
        &[include_str!("../assets/shaders/deferred_render.vert")],
    ).chain_err(|| "Failed to compile draw texture vertex shader")?;

    let draw_texture_fragment = Shader::new(
        ShaderType::Fragment,
        &[
            include_str!("../assets/shaders/libs/version.glsl"),
            include_str!("../assets/shaders/libs/consts.glsl"),
            include_str!("../assets/shaders/atmosphere/structures.glsl"),
            include_str!("../assets/shaders/atmosphere/constants.glsl"),
            "#define MALLUMO_ATMOSPHERE_GLOBALS 1\n",
            include_str!("../assets/shaders/atmosphere/render.glsl"),
            include_str!("../assets/shaders/libs/filmic_tonemapping.glsl"),
            include_str!("../assets/shaders/clouds/mix_clouds_atmosphere.frag"),
        ],
    ).chain_err(|| "Failed to compile draw texture fragment shader")?;

    let draw_texture_pipeline = PipelineBuilder::new()
        .vertex_shader(&draw_texture_vertex)
        .fragment_shader(&draw_texture_fragment)
        .build()
        .chain_err(|| "Unable to build draw texture pipeline")?;

    let camera = SphereCamera::new(
        SpherePosition {
            radius: 1.0,
            y_angle: Rad(4.8),
            height_angle: Rad(2.25),
        },
        Point3::new(0.0, 0.0, 0.0),
        Vector3::new(0.0, 1.0, 0.0),
        Deg(90.0).into(),
        app.width as usize,
        app.height as usize,
        0.0001,
        100.0,
    ).chain_err(|| "Could not create camera")?;

    let sun_dir = [sun_to_dir(Rad(1.3419969), Rad(0.72532487))];

    for _ in 0..runs {
        timer_query.begin();

        app.renderer.clear_default_framebuffer(ClearBuffers::ColorDepth);

        {
            let dtt = DrawTextureTarget {
                color0: DrawTextureAttachOption::AttachTexture(&mut clouds_texture),
                ..Default::default()
            };
            let draw_command = DrawCommand::arrays(&raymarch_pipeline, 0, 3)
                .framebuffer(&clouds_fbo)
                .attachments(&dtt)
                .uniform(camera.get_buffer(), 0)
                // reprojecting
                .uniform_2f(app.width as f32, app.height as f32, 1)
                .uniform_1f(reprojection_module.sub_pixel_size as f32, 2)
                .uniform_1f(reprojection_module.get_current_pixel() as f32, 3)
                // clouds
                .uniform_1f(max_iterations as f32, 4)
                .texture_3d(&perlin_worley_noise, 0)
                .texture_3d(&worley_noise, 1)
                .texture_2d(&curl_noise, 2)
                .texture_2d(&coverage_texture, 3)
                // atmosphere
                .uniform(&atmosphere_module.globals_buffer, 1)
                .uniform_3fv(&sun_dir, 0)
                .texture_2d(&atmosphere_module.transmittance_texture, 4)
                .texture_3d(&atmosphere_module.scattering_texture, 5)
                .texture_2d(&atmosphere_module.irradiance_texture, 6);

            app.renderer
                .draw(&draw_command)
                .chain_err(|| "Could not render clouds")?;
        }

        reprojection_module
            .reproject(&mut app.renderer, &clouds_texture, &camera)
            .chain_err(|| "Could not reproject")?;

        {
            let draw_command = DrawCommand::arrays(&draw_texture_pipeline, 0, 3)
                .uniform(camera.get_buffer(), 0)
                .texture_2d(&reprojection_module.accumulator, 0)
                // atmosphere
                .uniform(&atmosphere_module.globals_buffer, 1)
                .uniform_3fv(&sun_dir, 0)
                .texture_2d(&atmosphere_module.transmittance_texture, 1)
                .texture_3d(&atmosphere_module.scattering_texture, 2)
                .texture_2d(&atmosphere_module.irradiance_texture, 3);

            app.renderer.draw(&draw_command).chain_err(|| "Could not draw texture")?;
        }

        app.swap_buffers();
        times.push(timer_query.end_ns());

        for _ in app.poll_events() {}
    }

    println!(
        "CLOUDS RENDER W:{:?} H:{:?} D:{:?} R:{:?} N:{:?}",
        width, height, downsample_factor, reprojection_factor, max_iterations
    );
    print_statistics(times);

    Ok(())
}
