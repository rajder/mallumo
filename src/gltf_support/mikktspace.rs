use std::os::raw::*;

#[repr(C)]
pub struct SMikkTSpaceInterface {
    pub get_faces_count: extern "C" fn(context: *const SMikkTSpaceContext) -> c_int,
    pub get_vertices_count: extern "C" fn(context: *const SMikkTSpaceContext, face: c_int) -> c_int,

    pub get_position: extern "C" fn(
        context: *const SMikkTSpaceContext,
        position_out: *mut c_float,
        face_index: c_int,
        vertex_index: c_int,
    ) -> (),
    pub get_normal: extern "C" fn(
        context: *const SMikkTSpaceContext,
        normal_out: *mut c_float,
        face_index: c_int,
        vertex_index: c_int,
    ) -> (),
    pub get_texture_coordinate: extern "C" fn(
        context: *const SMikkTSpaceContext,
        texture_coordinate_out: *mut c_float,
        face_index: c_int,
        vertex_index: c_int,
    ) -> (),

    pub set_tspace_basic: extern "C" fn(
        context: *const SMikkTSpaceContext,
        tangent: *const c_float,
        sign: c_float,
        face_index: c_int,
        vertex_index: c_int,
    ) -> (),
    pub set_tspace: extern "C" fn(
        context: *const SMikkTSpaceContext,
        tangent: *const c_float,
        bitangent: *const c_float,
        mag_s: c_float,
        mag_t: c_float,
        orientation_preserving: c_int,
        face_index: c_int,
        vertex_index: c_int,
    ) -> (),
}

#[repr(C)]
pub struct SMikkTSpaceContext {
    pub interface: *mut SMikkTSpaceInterface,
    pub data: *mut c_void,
}

extern "C" {
    pub fn genTangSpaceDefault(context: *const SMikkTSpaceContext) -> c_int;
    pub fn genTangSpace(context: *const SMikkTSpaceContext, angular_hreshold: c_float) -> c_int;
}
