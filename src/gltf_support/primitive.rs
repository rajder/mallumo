use cgmath;
use image;

use gltf_support::Vertex;

#[derive(Debug, Clone)]
pub struct Primitive {
    pub vertices: Vec<Vertex>,
    pub indices: Vec<u32>,

    pub albedo: cgmath::Vector4<f32>,
    pub metallic: f32,
    pub roughness: f32,
    pub emission: cgmath::Vector4<f32>,

    pub albedo_texture: Option<image::RgbaImage>,
    pub metallic_roughness_texture: Option<image::GrayAlphaImage>,
    pub occlusion_texture: Option<image::GrayImage>,
    pub normal_texture: Option<image::RgbImage>,
    pub emissive_texture: Option<image::RgbImage>,
}
