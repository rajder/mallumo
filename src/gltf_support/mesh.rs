use gltf_support::primitive::*;

#[derive(Debug, Clone)]
pub struct Mesh {
    pub primitives: Vec<Primitive>,
}
