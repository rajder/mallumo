use super::*;

use mallumo_gls::*;
use shader_loader::*;

const NODEPOOL_MAX_SIZE: usize = 8_388_608; // 8MB

/// Sparse octree of scene in whose nodes are pointers into brickpool.
pub struct NodepoolModule {
    count_buffer: MutableBuffer,
}

pub struct Nodepool {
    pub count: usize,

    /// Length of nodes which are not leaf, hence those which can be mipmapped
    pub count_mip: usize,

    pub next_buffer: MutableBuffer,
    pub position_buffer: MutableBuffer,

    pub neighbours_x_buffer: MutableBuffer,
    pub neighbours_y_buffer: MutableBuffer,
    pub neighbours_z_buffer: MutableBuffer,
}

impl NodepoolModule {
    /// Creates new Nodepool Module.
    pub fn new() -> Result<NodepoolModule> {
        let count_buffer = MutableBuffer::new(&[0u32]).chain_err(|| "Could not create atomic count buffer")?;

        Ok(NodepoolModule {
            count_buffer: count_buffer,
        })
    }
}

impl NodepoolModule {
    /// Creates new Nodepool from passed Voxel Fragment List.
    pub fn create_nodepool(
        &mut self,
        renderer: &mut Renderer,
        options: &VXGIOptions,
        options_buffer: &MutableBuffer,
        voxel_fragment_list: &VoxelFragmentList,
    ) -> Result<Nodepool> {
        let mut next_buffer = MutableBuffer::new_empty(NODEPOOL_MAX_SIZE * ::std::mem::size_of::<u32>())
            .chain_err(|| "Could not create next_buffer")?;
        next_buffer
            .clear_data()
            .chain_err(|| "Could not clear next buffer data")?;

        let mut position_buffer = MutableBuffer::new_empty(4 * NODEPOOL_MAX_SIZE * ::std::mem::size_of::<f32>())
            .chain_err(|| "Could not create neighbour buffer")?;
        position_buffer
            .set_sub_data(&[0.5f32, 0.5f32, 0.5f32, 0.0f32], 0)
            .chain_err(|| "Could not update position buffer")?;

        let mut neighbours_x_buffer = MutableBuffer::new_empty(NODEPOOL_MAX_SIZE * ::std::mem::size_of::<u32>())
            .chain_err(|| "Could not create neighbour buffer")?;
        let mut neighbours_y_buffer = MutableBuffer::new_empty(NODEPOOL_MAX_SIZE * ::std::mem::size_of::<u32>())
            .chain_err(|| "Could not create neighbour buffer")?;
        let mut neighbours_z_buffer = MutableBuffer::new_empty(NODEPOOL_MAX_SIZE * ::std::mem::size_of::<u32>())
            .chain_err(|| "Could not create neighbour buffer")?;

        neighbours_x_buffer
            .clear_data()
            .chain_err(|| "Could not clear neighbours buffer data")?;
        neighbours_y_buffer
            .clear_data()
            .chain_err(|| "Could not clear neighbours buffer data")?;
        neighbours_z_buffer
            .clear_data()
            .chain_err(|| "Could not clear neighbours buffer data")?;

        let mut node_allocated: usize = 1;

        self.count_buffer
            .set_sub_data(&[0u32], 0)
            .chain_err(|| "Could not update atomic count buffer")?;

        let mut count_mip = 0;

        for level in 0..options.levels() {
            // Node neighbours
            if level > 0 {
                let node_neighbours_pipeline = &*fetch_program("vxgi/svo/voxelize/node_neighbours");
                let mut node_neighbours_command = DrawCommand::arrays(node_neighbours_pipeline, 0, node_allocated)
                        .mode(DrawMode::Points)
                        // Options
                        .uniform(options_buffer, 0)
                        // Current level
                        .uniform_1ui(level as u32, 0)
                        // Nodepool
                        .storage(&next_buffer, 0)
                        .storage(&position_buffer, 1)
                        // Nodepool Neighbours
                        .storage(&neighbours_x_buffer, 2)
                        .storage(&neighbours_y_buffer, 3)
                        .storage(&neighbours_z_buffer, 4)
                        .barriers(MemoryBarriers::All);
                renderer
                    .draw(&node_neighbours_command)
                    .chain_err(|| "Could not set neighbours")?;
            }

            // Node flag
            {
                let node_flag_pipeline = &*fetch_program("vxgi/svo/voxelize/node_flag");
                let node_flag_command = DrawCommand::arrays(node_flag_pipeline, 0, voxel_fragment_list.count)
                    .mode(DrawMode::Points)
                    .uniform(options_buffer, 0)
                    .uniform_1ui(level as u32, 0)
                    .storage(&voxel_fragment_list.voxel_positions_buffer, 0)
                    .storage(&next_buffer, 1)
                    .storage(&position_buffer, 2)
                    .barriers(MemoryBarriers::All);

                renderer.draw(&node_flag_command).chain_err(|| "Could not flag nodes")?;
            }

            // Node Allocation Pass
            {
                let node_allocation_pipeline = &*fetch_program("vxgi/svo/voxelize/node_allocate");
                let node_allocation_command = DrawCommand::arrays(node_allocation_pipeline, 0, node_allocated)
                    .mode(DrawMode::Points)
                    .uniform(options_buffer, 0)
                    .uniform_1ui(level as u32, 0)
                    .storage(&next_buffer, 0)
                    .storage(&position_buffer, 1)
                    .atomic_counter(&self.count_buffer, 1)
                    .barriers(MemoryBarriers::All);

                renderer
                    .draw(&node_allocation_command)
                    .chain_err(|| "Could not allocate nodes")?;
            }

            // Retrieve how many nodes were split
            let mut allocated_tiles = self
                .count_buffer
                .get::<u32>(1, 0)
                .chain_err(|| "Could not retrieve number of allocated tiles")?;

            // Node offset moves to the end of the processed node list
            node_allocated = allocated_tiles[0] as usize * 8 + 1;

            if level == options.levels() - 2 {
                count_mip = node_allocated;
            }
        }

        let count = node_allocated;

        Ok(Nodepool {
            count: count,
            count_mip: count_mip,

            next_buffer: next_buffer,
            position_buffer: position_buffer,

            neighbours_x_buffer: neighbours_x_buffer,
            neighbours_y_buffer: neighbours_y_buffer,
            neighbours_z_buffer: neighbours_z_buffer,
        })
    }
}

/*
impl<'a> Nodepool<'a> {
    pub fn render_debug(
        &mut self,
        renderer: &mut Renderer,
        camera: &Camera,
        level: usize,
    ) -> Result<()> {
        renderer.set_enable(EnableOption::DepthTest);
        renderer.set_enable(EnableOption::CullFace);

        self.globals_buffer
            .set_sub_data(&[level as u32], 8)
            .chain_err(|| "Could not update globals buffer with levels variable")?;

        let draw_command = DrawCommand::arrays(
            self.node_visualization_pipeline,
            start,
            self.count - start,
        ).mode(DrawMode::Points)
            .uniform(&self.globals_buffer, 0)
            .uniform(camera.get_buffer(), 1)
            .storage_read(&self.next_buffer, 0)
            .storage_read(&self.position_buffer, 1);

        renderer
            .draw(&draw_command)
            .chain_err(|| "Could not draw nodepool debug")?;

        Ok(())
    }
}
*/
