use super::*;

use gltf_support::*;
use mallumo_gls::*;
use shader_loader::*;
use shape_list::*;

pub struct VoxelFragmentListModule {
    framebuffer: GeneralFramebuffer,
    count_buffer: MutableBuffer,
    count: usize,

    options: VXGIOptions,
}

pub struct VoxelFragmentList {
    pub count: usize,

    pub voxel_positions_buffer: MutableBuffer,
    pub voxel_albedos_buffer: MutableBuffer,
    pub voxel_normals_buffer: MutableBuffer,
    pub voxel_emissions_buffer: MutableBuffer,
}

impl VoxelFragmentListModule {
    pub fn new(options: VXGIOptions) -> Result<VoxelFragmentListModule> {
        let count_buffer =
            MutableBuffer::new(&[0 as u32]).chain_err(|| "Could not create voxel fragment list count buffer")?;

        let mut framebuffer = GeneralFramebuffer::new();
        framebuffer.set_viewport(Viewport {
            x: 0,
            y: 0,
            width: options.dimension() as usize,
            height: options.dimension() as usize,
        });
        framebuffer.set_parameter(FramebufferParameter::DefaultWidth, options.dimension() as u32);
        framebuffer.set_parameter(FramebufferParameter::DefaultHeight, options.dimension() as u32);
        framebuffer.set_disable(EnableOption::DepthTest);
        framebuffer.set_disable(EnableOption::CullFace);
        framebuffer.set_clip_control_depth(ClipControlDepth::NegativeOneToOne);

        Ok(VoxelFragmentListModule {
            framebuffer: framebuffer,
            count_buffer: count_buffer,
            count: 0,

            options: options,
        })
    }

    pub fn voxelize(
        &mut self,
        renderer: &mut Renderer,
        options_buffer: &MutableBuffer,
        shape_lists: &[&ShapeList],
    ) -> Result<VoxelFragmentList> {
        let buffer_size: usize = {
            self.count = 0;
            self.count_buffer
                .set_sub_data(&[0u32], 0)
                .chain_err(|| "Could not update atomic count buffer")?;

            let count_voxels_pipeline = &*fetch_program("vxgi/svo/voxelize/voxelize_count");

            for shape_list in shape_lists.iter() {
                for (i, shape) in shape_list.shapes.iter().enumerate() {
                    let indices = shape.indices;
                    let vertices = shape.vertices;

                    let draw_command = DrawCommand::arrays(count_voxels_pipeline, 0, shape.indices.1)
                        .framebuffer(&self.framebuffer)
                        .uniform(options_buffer, 0)
                        .storage_range_read::<u32>(&shape_list.indices_buffer, 1, indices.0, indices.1)
                        .storage_range_read::<Vertex>(&shape_list.vertices_buffer, 2, vertices.0, vertices.1)
                        .storage_range_read::<PrimitiveParameters>(&shape_list.primitive_parameters_buffer, 3, i, 1)
                        .atomic_counter(&self.count_buffer, 0)
                        .barriers(MemoryBarriers::All);

                    renderer.draw(&draw_command).chain_err(|| "Could not count voxels")?;
                }
            }

            self.count = self
                .count_buffer
                .get::<u32>(1, 0)
                .chain_err(|| "Could not get value of atomic buffer")?[0] as usize;

            self.count * ::std::mem::size_of::<u32>()
        };

        let store_voxels_pipeline_name = match self.options.hdr() {
            true => "vxgi/svo/voxelize/voxelize_rgba32",
            false => "vxgi/svo/voxelize/voxelize_rgba8",
        };
        let store_voxels_pipeline = &*fetch_program(store_voxels_pipeline_name);

        // Allocate space for voxel data
        let voxel_positions_buffer =
            MutableBuffer::new_empty(buffer_size).chain_err(|| "Could not create voxel positions buffer")?;
        let voxel_albedos_buffer =
            MutableBuffer::new_empty(buffer_size).chain_err(|| "Could not create voxel albedos buffer")?;
        let voxel_normals_buffer =
            MutableBuffer::new_empty(buffer_size).chain_err(|| "Could not create voxel albedos buffer")?;
        let voxel_emissions_buffer =
            MutableBuffer::new_empty(buffer_size).chain_err(|| "Could not create voxel emissions buffer")?;

        self.count_buffer
            .set_sub_data(&[0u32], 0)
            .chain_err(|| "Could not update atomic count buffer")?;

        // Store voxels
        for shape_list in shape_lists.iter() {
            for (i, shape) in shape_list.shapes.iter().enumerate() {
                let indices = shape.indices;
                let vertices = shape.vertices;

                let draw_command = DrawCommand::arrays(store_voxels_pipeline, 0, shape.indices.1)
                    .framebuffer(&self.framebuffer)
                    .uniform(options_buffer, 0)
                    .storage_range_read::<u32>(&shape_list.indices_buffer, 1, indices.0, indices.1)
                    .storage_range_read::<Vertex>(&shape_list.vertices_buffer, 2, vertices.0, vertices.1)
                    .storage_range_read::<PrimitiveParameters>(&shape_list.primitive_parameters_buffer, 3, i, 1)
                    .texture_2d(shape_list.texture(shape.albedo), 0)
                    .texture_2d(shape_list.texture(shape.metallic_roughness), 2)
                    .texture_2d(shape_list.texture(shape.occlusion), 3)
                    .texture_2d(shape_list.texture(shape.emissive), 4)
                    .storage(&voxel_positions_buffer, 4)
                    .storage(&voxel_albedos_buffer, 5)
                    .storage(&voxel_normals_buffer, 6)
                    .storage(&voxel_emissions_buffer, 7)
                    .atomic_counter(&self.count_buffer, 0)
                    .barriers(MemoryBarriers::All);
                renderer.draw(&draw_command).chain_err(|| "Could not store voxels")?;
            }
        }

        self.count = self
            .count_buffer
            .get::<u32>(1, 0)
            .chain_err(|| "Could not get value of atomic buffer")?[0] as usize;

        Ok(VoxelFragmentList {
            count: self.count,

            voxel_positions_buffer: voxel_positions_buffer,
            voxel_albedos_buffer: voxel_albedos_buffer,
            voxel_normals_buffer: voxel_normals_buffer,
            voxel_emissions_buffer: voxel_emissions_buffer,
        })
    }
}
