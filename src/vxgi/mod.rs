mod errors;
use errors::*;

mod options;
pub use self::options::*;

mod options_gui;
pub use self::options_gui::*;

mod options_arguments;
pub use self::options_arguments::*;

mod voxel_texture;
pub use self::voxel_texture::*;

mod sparse_voxel_octree;
pub use self::sparse_voxel_octree::*;

mod global_illumination;
pub use self::global_illumination::*;

use camera::*;
use deferred::*;
use mallumo_gls::*;
use shape_list::*;
use sun::*;

/// Determines the type of data of 3D voxel texture.
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
#[repr(u32)]
pub enum VoxelTextureType {
    Albedo = 0u32,
    Normal = 1u32,
    Emission = 2u32,
    MetallicRoughness = 3u32,
    Radiance = 4u32,
}

///
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum VoxelTextureFormat {
    RGBA8,
    RGBA32,
}

impl From<VoxelTextureFormat> for TextureInternalFormat {
    fn from(voxel_texture_format: VoxelTextureFormat) -> TextureInternalFormat {
        match voxel_texture_format {
            VoxelTextureFormat::RGBA32 => TextureInternalFormat::RGBA32F,
            VoxelTextureFormat::RGBA8 => TextureInternalFormat::RGBA8,
        }
    }
}

impl From<VoxelTextureFormat> for ImageInternalFormat {
    fn from(voxel_texture_format: VoxelTextureFormat) -> ImageInternalFormat {
        match voxel_texture_format {
            VoxelTextureFormat::RGBA32 => ImageInternalFormat::RGBA32F,
            VoxelTextureFormat::RGBA8 => ImageInternalFormat::RGBA8,
        }
    }
}

impl From<VoxelTextureFormat> for TextureFormat {
    fn from(_: VoxelTextureFormat) -> TextureFormat {
        TextureFormat::RGBA
    }
}

pub trait VXGIModule {
    /// Voxelizes given scene.
    fn voxelize(&mut self, renderer: &mut Renderer, shape_lists: &[&ShapeList], static_geometry: bool) -> Result<()>;

    /// Injects Sun light into voxels.
    /// Suns last calculated shadowmap will be used. It is caller's responsibility to update shadowmap.
    fn inject_radiance(&mut self, renderer: &mut Renderer, sun: &Sun) -> Result<()>;

    fn render_voxels(&self, renderer: &mut Renderer, camera: &Camera, level: usize, ty: VoxelTextureType)
        -> Result<()>;

    /// Renders scene with given mode into main framebuffer.
    /// Last result of deffered collector will be used. It is caller's responsibility to update deferred.
    fn render(
        &mut self,
        renderer: &mut Renderer,
        deferred_collector: &DefferedCollector,
        camera: &Camera,
        sun: &Sun,
    ) -> Result<()>;

    fn update_options(&mut self, options: VXGIOptions) -> Result<()>;
}
