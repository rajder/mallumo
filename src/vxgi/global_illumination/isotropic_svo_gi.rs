use super::*;

use camera::*;
use mallumo_gls::*;
use shader_loader::*;
use shape_list::*;

pub struct IsotropicSVOGI {
    voxel_fragment_list_module: VoxelFragmentListModule,
    voxel_fragment_list: Option<VoxelFragmentList>,

    nodepool_module: NodepoolModule,
    nodepool: Option<Nodepool>,

    brickpool: Option<Brickpool>,

    options: VXGIOptions,
    options_buffer: MutableBuffer,
}

impl IsotropicSVOGI {
    pub fn new(options: VXGIOptions) -> Result<IsotropicSVOGI> {
        let voxel_fragment_list_module =
            VoxelFragmentListModule::new(options).chain_err(|| "Could not create voxel fragment list module")?;

        let nodepool_module = NodepoolModule::new().chain_err(|| "Could not create nodepool module")?;

        let options_buffer = MutableBuffer::new(&[options]).chain_err(|| "Could not create options buffer")?;

        Ok(IsotropicSVOGI {
            voxel_fragment_list_module: voxel_fragment_list_module,
            voxel_fragment_list: None,

            nodepool_module: nodepool_module,
            nodepool: None,

            brickpool: None,

            options: options,
            options_buffer: options_buffer,
        })
    }

    pub fn voxel_fragment_list(&self) -> &VoxelFragmentList {
        self.voxel_fragment_list.as_ref().unwrap()
    }

    pub fn nodepool(&self) -> &Nodepool {
        self.nodepool.as_ref().unwrap()
    }

    pub fn brickpool(&self) -> &Brickpool {
        self.brickpool.as_ref().unwrap()
    }

    pub fn render_point_cloud(&mut self, renderer: &mut Renderer, camera: &Camera) -> Result<()> {
        renderer.set_enable(EnableOption::ProgramPointSize);

        let point_cloud_pipeline = &*fetch_program("vxgi/svo/visualize/fragment_list");

        let voxel_fragment_list = self.voxel_fragment_list.as_ref().unwrap();

        let render_point_cloud_command = DrawCommand::arrays(point_cloud_pipeline, 0, voxel_fragment_list.count)
            .mode(DrawMode::Points)
            .uniform(&self.options_buffer, 0)
            .uniform(camera.get_buffer(), 1)
            .storage(&voxel_fragment_list.voxel_positions_buffer, 0)
            .storage(&voxel_fragment_list.voxel_albedos_buffer, 1);

        renderer
            .draw(&render_point_cloud_command)
            .chain_err(|| "Could not render point cloud")?;

        Ok(())
    }

    pub fn render_nodepool(&mut self, renderer: &mut Renderer, camera: &Camera) -> Result<()> {
        let nodepool_pipeline = &*fetch_program("vxgi/svo/visualize/nodepool");

        let nodepool = self.nodepool.as_ref().unwrap();

        let render_nodepool_command = DrawCommand::arrays(nodepool_pipeline, 0, nodepool.count)
            .mode(DrawMode::Points)
            .uniform(&self.options_buffer, 0)
            .uniform(camera.get_buffer(), 1)
            .storage(&nodepool.position_buffer, 0);

        renderer
            .draw(&render_nodepool_command)
            .chain_err(|| "Could not render point cloud")?;

        Ok(())
    }

    pub fn render_brickpool(
        &mut self,
        renderer: &mut Renderer,
        camera: &Camera,
        level: usize,
        ty: BrickpoolType,
    ) -> Result<()> {
        self.brickpool
            .as_ref()
            .unwrap()
            .render_debug(renderer, camera, self.nodepool.as_ref().unwrap(), level, ty)
            .chain_err(|| "")?;

        Ok(())
    }
}

impl VXGIModule for IsotropicSVOGI {
    fn voxelize(&mut self, renderer: &mut Renderer, shape_lists: &[&ShapeList], static_geometry: bool) -> Result<()> {
        if !static_geometry {
            return Ok(());
        }

        self.voxel_fragment_list = Some(self
            .voxel_fragment_list_module
            .voxelize(renderer, &self.options_buffer, shape_lists)
            .chain_err(|| "Could not create voxel fragment list")?);

        self.nodepool = Some(self
            .nodepool_module
            .create_nodepool(
                renderer,
                &self.options,
                &self.options_buffer,
                self.voxel_fragment_list.as_ref().unwrap(),
            )
            .chain_err(|| "Could not create nodepool")?);

        self.brickpool = Some(Brickpool::new(
            renderer,
            self.voxel_fragment_list.as_ref().unwrap(),
            self.nodepool.as_mut().unwrap(),
            &self.options,
            &self.options_buffer,
        ).chain_err(|| "")?);

        Ok(())
    }

    fn inject_radiance(&mut self, renderer: &mut Renderer, sun: &Sun) -> Result<()> {
        let brickpool = self.brickpool.as_mut().unwrap();
        let nodepool = self.nodepool.as_ref().unwrap();

        let sun_positions = sun.get_positions();
        let sun_buffer = sun.get_buffer();

        brickpool
            .irradiances
            .clear()
            .chain_err(|| "Could not clear irradiance")?;

        // Inject light
        {
            let pipeline_name = match self.options.hdr() {
                true => "vxgi/svo/inject_radiance_rgba32",
                false => "vxgi/svo/inject_radiance",
            };

            let format = match self.options.hdr() {
                true => ImageInternalFormat::RGBA32F,
                false => ImageInternalFormat::RGBA8,
            };

            let inject_light_pipeline = &*fetch_program(pipeline_name);

            let inject_light_command = DrawCommand::arrays(
                inject_light_pipeline,
                0,
                (sun_positions.size.0 * sun_positions.size.1) as usize,
            ).mode(DrawMode::Points)
                .uniform(&self.options_buffer, 0)
                .uniform(sun_buffer, 1)
                .storage(&nodepool.next_buffer, 0)
                .texture_2d(sun_positions, 1)
                .image(&brickpool.albedos, 0, 0, format)
                .image(&brickpool.irradiances, 1, 0, format)
                .barriers(MemoryBarriers::All);

            renderer
                .draw(&inject_light_command)
                .chain_err(|| "Could not inject light")?;
        }

        brickpool
            .spread_leaf(
                renderer,
                nodepool,
                self.options.levels() as usize - 1usize,
                BrickpoolType::Irradiance,
            )
            .chain_err(|| "Could not spread leaf irradiance")?;
        brickpool
            .mipmap(renderer, nodepool, BrickpoolType::Irradiance)
            .chain_err(|| "Could not mipmap irradiance")?;

        Ok(())
    }

    fn render_voxels(
        &self,
        _renderer: &mut Renderer,
        _camera: &Camera,
        _level: usize,
        _ty: VoxelTextureType,
    ) -> Result<()> {
        unimplemented!();
    }

    fn render(
        &mut self,
        renderer: &mut Renderer,
        deferred_collector: &DefferedCollector,
        camera: &Camera,
        sun: &Sun,
    ) -> Result<()> {
        let pipeline = &*fetch_program("vxgi/svo/isotropic_gi");

        let brickpool = self.brickpool.as_ref().unwrap();
        let nodepool = self.nodepool.as_ref().unwrap();

        let render_command = DrawCommand::arrays(pipeline, 0, 3)
            // Global uniforms
            .uniform(&self.options_buffer, 0)
            .uniform(camera.get_buffer(), 1)
            .uniform(sun.get_buffer(), 2)
            .texture_2d(sun.get_shadowmap(), 5)
            // Deferred G-buffer
            .texture_2d(&deferred_collector.position, 0)
            .texture_2d(&deferred_collector.albedo, 1)
            .texture_2d(&deferred_collector.orm, 2)
            .texture_2d(&deferred_collector.normal, 3)
            // Nodepool
            .storage(&nodepool.next_buffer, 0)
            // Brickpool
            .texture_3d(&brickpool.albedos, 6)
            .texture_3d(&brickpool.irradiances, 7);

        renderer.draw(&render_command).chain_err(|| "Could not render GI")?;

        Ok(())
    }

    fn update_options(&mut self, options: VXGIOptions) -> Result<()> {
        self.options = options;
        self.options_buffer
            .set_data::<VXGIOptions>(&[options])
            .chain_err(|| "Could not update options buffer")?;

        Ok(())
    }
}
