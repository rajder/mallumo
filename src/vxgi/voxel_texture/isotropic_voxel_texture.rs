use mallumo_gls::*;
use shader_loader::*;
use vxgi::*;

#[derive(Debug)]
pub struct IsotropicVoxelTexture {
    textures: Vec<Texture3D>,

    size: usize,
    levels: usize,

    voxel_format: VoxelTextureFormat,
}

impl IsotropicVoxelTexture {
    pub fn new(levels: usize, format: VoxelTextureFormat) -> Result<IsotropicVoxelTexture> {
        let parameters = Texture3DParameters {
            min: TextureTexelFilter::Linear,
            mag: TextureTexelFilter::Linear,
            mipmap: TextureMipmapFilter::None,
            wrap_s: TextureWrapMode::ClampToEdge,
            wrap_t: TextureWrapMode::ClampToEdge,
            wrap_r: TextureWrapMode::ClampToEdge,
        };

        let mut textures: Vec<Texture3D> = Vec::new();
        for i in 0..levels + 1 {
            let size = 2usize.pow(levels as u32 - i as u32);

            let texture = Texture3D::new_empty(
                Texture3DSize(size, size, size),
                format.into(),
                TextureFormat::RGBA,
                TextureDataType::Float,
                parameters,
                1,
            ).chain_err(|| "Could not create texture")?;

            textures.push(texture);
        }

        Ok(IsotropicVoxelTexture {
            textures: textures,

            size: 2usize.pow(levels as u32),
            levels: levels,

            voxel_format: format,
        })
    }

    pub fn level(&self, level: usize) -> &Texture3D {
        &self.textures[level]
    }

    pub fn generate_mipmap(&mut self, renderer: &mut Renderer) -> Result<()> {
        let pipeline_name = match self.voxel_format {
            VoxelTextureFormat::RGBA8 => "vxgi/voxel_texture/mipmap/isotropic_mipmap_rgba8",
            VoxelTextureFormat::RGBA32 => "vxgi/voxel_texture/mipmap/isotropic_mipmap_rgba32",
        };

        let pipeline = &*fetch_compute_program(pipeline_name);

        let image_internal_format = match self.voxel_format {
            VoxelTextureFormat::RGBA8 => ImageInternalFormat::RGBA8,
            VoxelTextureFormat::RGBA32 => ImageInternalFormat::RGBA32F,
        };

        let mut size = self.size / 2;
        for level in 0..self.levels - 1 {
            let workgroup_size = (size + 7) / 8;
            let command = DrawCommand::compute(pipeline, workgroup_size, workgroup_size, workgroup_size)
                .uniform_1ui(size as u32, 0)
                .texture_3d(&self.textures[level], 0)
                .image_3d(&self.textures[level + 1], 1, 0, image_internal_format)
                .barriers(MemoryBarriers::All);

            renderer.draw(&command).chain_err(|| "Could not mipmap texture")?;

            size /= 2;
        }

        Ok(())
    }

    pub fn clear(&mut self) -> Result<()> {
        for level in 0..self.levels + 1 {
            self.clear_mipmap(level)?;
        }

        Ok(())
    }

    pub fn clear_mipmap(&mut self, level: usize) -> Result<()> {
        self.textures[level]
            .clear()
            .chain_err(|| format!("Could not clear mipmap level {}", level))?;
        Ok(())
    }
}
