use super::errors::*;
use super::*;

use cgmath::{Deg, Matrix4, Point3, Vector3};

/// Creates diffuse and speculer IBL cubemap Texture.
pub fn create_diffuse_specular_ibl(
    mut renderer: &mut Renderer,
    environment_cubemap: &TextureCubemap,
    diffuse_size: usize,
    specular_size: usize,
) -> Result<(TextureCubemap, TextureCubemap)> {
    let diffuse = convolute_irradiance(&mut renderer, environment_cubemap, diffuse_size)
        .chain_err(|| "Could not convolute irradiance map")?;

    let specular =
        prefilter_map(&mut renderer, environment_cubemap, specular_size).chain_err(|| "Could not prefilter cubemap")?;

    Ok((diffuse, specular))
}

/// Crates diffuse IBL cubemap texture.
pub fn convolute_irradiance(
    renderer: &mut Renderer,
    environment_cubemap: &TextureCubemap,
    size: usize,
) -> Result<TextureCubemap> {
    // Create pipeline
    let vertex_shader = Shader::new(
        ShaderType::Vertex,
        &[include_str!(
            "../assets/shaders/image_based_lighting/diffuse_irradiance_convolution_vertex.glsl"
        )],
    ).chain_err(|| "Failed to compile vertex shader")?;

    let fragment_shader = Shader::new(
        ShaderType::Fragment,
        &[include_str!(
            "../assets/shaders/image_based_lighting/diffuse_irradiance_convolution_fragment.glsl"
        )],
    ).chain_err(|| "Failed to compile fragment shader")?;

    let pipeline = PipelineBuilder::new()
        .vertex_shader(&vertex_shader)
        .fragment_shader(&fragment_shader)
        .build()
        .chain_err(|| "Unable to build pipeline")?;

    // Create cube
    let cube_vertices_buffer = ImmutableBuffer::new(&CUBE_VERTICES).chain_err(|| "Unable to create vertices buffer")?;
    let cube_indices_buffer = ImmutableBuffer::new(&CUBE_INDICES).chain_err(|| "Unable to create indices buffer")?;

    // Create texture
    let mut irradiance_texture = TextureCubemap::new_empty(
        TextureCubemapSize(size, size),
        TextureInternalFormat::RGB32F,
        TextureFormat::RGB,
        TextureDataType::Float,
        TextureCubemapParameters {
            min: TextureTexelFilter::Linear,
            mag: TextureTexelFilter::Linear,
            wrap_s: TextureWrapMode::ClampToEdge,
            wrap_t: TextureWrapMode::ClampToEdge,
            wrap_r: TextureWrapMode::ClampToEdge,
            seamless: TextureSeamless::True,
            mipmap: TextureMipmapFilter::Linear,
        },
        1,
    ).chain_err(|| "Unable to crate cubemap")?;

    // Create framebuffer
    let mut framebuffer = GeneralFramebuffer::new();
    framebuffer.set_viewport(Viewport {
        x: 0,
        y: 0,
        width: size,
        height: size,
    });
    framebuffer.set_clear_color(ClearColor::default());
    framebuffer.set_disable(EnableOption::DepthTest);

    // Create buffers
    let perspective = cgmath::perspective(Deg(90.0), 1.0 as f32, 0.1 as f32, 10.0 as f32);
    let center = Point3::new(0.0f32, 0.0, 0.0);
    let views = [
        Matrix4::look_at(center, Point3::new(1.0, 0.0, 0.0), Vector3::new(0.0, -1.0, 0.0)),
        Matrix4::look_at(center, Point3::new(-1.0, 0.0, 0.0), Vector3::new(0.0, -1.0, 0.0)),
        Matrix4::look_at(center, Point3::new(0.0, 1.0, 0.0), Vector3::new(0.0, 0.0, 1.0)),
        Matrix4::look_at(center, Point3::new(0.0, -1.0, 0.0), Vector3::new(0.0, 0.0, -1.0)),
        Matrix4::look_at(center, Point3::new(0.0, 0.0, 1.0), Vector3::new(0.0, -1.0, 0.0)),
        Matrix4::look_at(center, Point3::new(0.0, 0.0, -1.0), Vector3::new(0.0, -1.0, 0.0)),
    ];

    let mut camera_buffer = MutableBuffer::new_empty(128).chain_err(|| "Unable to create camera buffer")?;
    camera_buffer
        .set_sub_data(perspective.as_ref() as &[f32; 16], 0)
        .chain_err(|| "Could not update camera buffer")?;

    // Render
    for i in 0..6 {
        // Update face where to render
        camera_buffer
            .set_sub_data(views[i].as_ref() as &[f32; 16], 64)
            .chain_err(|| "Coult not change view")?;

        let attachments = DrawTextureTarget {
            color0: DrawTextureAttachOption::AttachTextureLayer {
                texture: &mut irradiance_texture,
                level: 0,
                layer: i,
            },
            ..Default::default()
        };

        let draw_command = DrawCommand::arrays(&pipeline, 0, CUBE_INDICES.len())
            .framebuffer(&framebuffer)
            .attachments(&attachments)
            .uniform(&camera_buffer, 0)
            .storage_read(&cube_indices_buffer, 0)
            .storage_read(&cube_vertices_buffer, 1)
            .texture_cubemap(&environment_cubemap, 0);

        renderer
            .draw(&draw_command)
            .chain_err(|| "Could not convolute irradiance")?;
    }

    Ok(irradiance_texture)
}

/// Creates specular IBL cubemap Texture.
pub fn prefilter_map(
    renderer: &mut Renderer,
    environment_cubemap: &TextureCubemap,
    size: usize,
) -> Result<TextureCubemap> {
    let levels = discrete_floored_log2(size);

    // Create pipeline
    let vertex_shader = Shader::new(
        ShaderType::Vertex,
        &[include_str!(
            "../assets/shaders/image_based_lighting/prefilter_vertex.glsl"
        )],
    ).chain_err(|| "Failed to compile vertex shader")?;

    let fragment_shader = Shader::new(
        ShaderType::Fragment,
        &[include_str!(
            "../assets/shaders/image_based_lighting/prefilter_fragment.glsl"
        )],
    ).chain_err(|| "Failed to compile fragment shader")?;

    let pipeline = PipelineBuilder::new()
        .vertex_shader(&vertex_shader)
        .fragment_shader(&fragment_shader)
        .build()
        .chain_err(|| "Unable to build pipeline")?;

    // Create cube
    let cube_vertices_buffer = ImmutableBuffer::new(&CUBE_VERTICES).chain_err(|| "Unable to create vertices buffer")?;
    let cube_indices_buffer = ImmutableBuffer::new(&CUBE_INDICES).chain_err(|| "Unable to create indices buffer")?;

    // Create framebuffer
    let mut framebuffer = GeneralFramebuffer::new();
    framebuffer.set_disable(EnableOption::DepthTest);

    // Create prefilter cubemap texture
    let mut prefilter_texture = TextureCubemap::new_empty(
        TextureCubemapSize(size, size),
        TextureInternalFormat::RGB16F,
        TextureFormat::RGB,
        TextureDataType::Float,
        TextureCubemapParameters {
            min: TextureTexelFilter::Linear,
            mag: TextureTexelFilter::Linear,
            wrap_s: TextureWrapMode::ClampToEdge,
            wrap_t: TextureWrapMode::ClampToEdge,
            wrap_r: TextureWrapMode::ClampToEdge,
            seamless: TextureSeamless::True,
            mipmap: TextureMipmapFilter::Linear,
        },
        levels,
    ).chain_err(|| "Unable to create cubemap")?;

    // Create buffers
    let perspective = cgmath::perspective(Deg(90.0), 1.0 as f32, 0.1 as f32, 10.0 as f32);
    let center = Point3::new(0.0f32, 0.0, 0.0);
    let views = [
        Matrix4::look_at(center, Point3::new(1.0, 0.0, 0.0), Vector3::new(0.0, -1.0, 0.0)),
        Matrix4::look_at(center, Point3::new(-1.0, 0.0, 0.0), Vector3::new(0.0, -1.0, 0.0)),
        Matrix4::look_at(center, Point3::new(0.0, 1.0, 0.0), Vector3::new(0.0, 0.0, 1.0)),
        Matrix4::look_at(center, Point3::new(0.0, -1.0, 0.0), Vector3::new(0.0, 0.0, -1.0)),
        Matrix4::look_at(center, Point3::new(0.0, 0.0, 1.0), Vector3::new(0.0, -1.0, 0.0)),
        Matrix4::look_at(center, Point3::new(0.0, 0.0, -1.0), Vector3::new(0.0, -1.0, 0.0)),
    ];

    let mut camera_buffer = MutableBuffer::new_empty(128).chain_err(|| "Unable to create camera buffer")?;
    camera_buffer
        .set_sub_data(perspective.as_ref() as &[f32; 16], 0)
        .chain_err(|| "Could not update camera buffer")?;

    // Globals Uniform Block
    let mut globals_buffer = MutableBuffer::new_empty(16).chain_err(|| "Unable to create globals buffer")?;

    // Render
    for level in 0..levels {
        let mip_width: usize = (size as f32 * f32::powf(0.5, level as f32)) as usize;
        let mip_height: usize = mip_width;
        let roughness = level as f32 / (levels - 1) as f32;

        globals_buffer
            .set_sub_data(&[roughness], 0)
            .chain_err(|| "Could not update texture handle")?;

        framebuffer.set_viewport(Viewport {
            x: 0,
            y: 0,
            width: mip_width,
            height: mip_height,
        });

        for layer in 0..6 {
            // Update face where to render
            camera_buffer
                .set_sub_data(views[layer].as_ref() as &[f32; 16], 64)
                .chain_err(|| "Could not change view matrix")?;

            let attachments = DrawTextureTarget {
                color0: DrawTextureAttachOption::AttachTextureLayer {
                    texture: &mut prefilter_texture,
                    level: level,
                    layer: layer,
                },
                ..Default::default()
            };

            let draw_command = DrawCommand::arrays(&pipeline, 0, CUBE_INDICES.len())
                .framebuffer(&framebuffer)
                .attachments(&attachments)
                .uniform(&camera_buffer, 0)
                .uniform(&globals_buffer, 1)
                .storage_read(&cube_indices_buffer, 0)
                .storage_read(&cube_vertices_buffer, 1)
                .texture_cubemap(&environment_cubemap, 0);

            renderer
                .draw(&draw_command)
                .chain_err(|| "Could not prefilter cubemap")?;
        }
    }

    Ok(prefilter_texture)
}

/// Creates BRDF integration(LUT) Texture.
pub fn integrate_brdf(renderer: &mut Renderer, size: usize) -> Result<Texture2D> {
    // Create pipeline
    let vertex_shader = Shader::new(
        ShaderType::Vertex,
        &[include_str!(
            "../assets/shaders/image_based_lighting/integrate_brdf_vertex.glsl"
        )],
    ).chain_err(|| "Failed to compile vertex shader")?;

    let fragment_shader = Shader::new(
        ShaderType::Fragment,
        &[include_str!(
            "../assets/shaders/image_based_lighting/integrate_brdf_fragment.glsl"
        )],
    ).chain_err(|| "Failed to compile fragment shader")?;

    let pipeline = PipelineBuilder::new()
        .vertex_shader(&vertex_shader)
        .fragment_shader(&fragment_shader)
        .build()
        .chain_err(|| "Unable to build pipeline")?;

    // Create quad
    let vertices: Vec<f32> = vec![
        // positions          // tex coords
        -1.0,
        -1.0,
        0.0,
        0.0,
        0.0,
        0.0,
        0.0,
        0.0, // bottom left corner
        -1.0,
        1.0,
        0.0,
        0.0,
        0.0,
        1.0,
        0.0,
        0.0, // top left corner
        1.0,
        1.0,
        0.0,
        0.0,
        1.0,
        1.0,
        0.0,
        0.0, // top right corner
        1.0,
        -1.0,
        0.0,
        0.0,
        1.0,
        0.0,
        0.0,
        0.0, // bottom right corner
    ];
    let indices: Vec<u32> = vec![0, 1, 2, 0, 2, 3];

    let vertices_buffer = ImmutableBuffer::new(&vertices).chain_err(|| "Unable to create vertices buffer")?;
    let indices_buffer = ImmutableBuffer::new(&indices).chain_err(|| "Unable to create indices buffer")?;

    // Create BRDF texture
    let mut result = Texture2D::new_empty(
        Texture2DSize(size, size),
        TextureInternalFormat::RGB32F,
        TextureFormat::RGB,
        TextureDataType::Float,
        Texture2DParameters {
            wrap_s: TextureWrapMode::ClampToEdge,
            wrap_t: TextureWrapMode::ClampToEdge,
            min: TextureTexelFilter::Linear,
            mag: TextureTexelFilter::Linear,
            ..Default::default()
        },
        1,
    ).chain_err(|| "Could not create BRDF texture")?;

    // Create FBO
    let mut framebuffer = GeneralFramebuffer::new();
    framebuffer.set_viewport(Viewport {
        x: 0,
        y: 0,
        width: size,
        height: size,
    });
    framebuffer.set_disable(EnableOption::DepthTest);

    // Draw
    {
        let attachments = DrawTextureTarget {
            color0: DrawTextureAttachOption::AttachTexture(&mut result),
            ..Default::default()
        };

        let draw_command = DrawCommand::arrays(&pipeline, 0, indices.len())
            .framebuffer(&framebuffer)
            .attachments(&attachments)
            .storage_read(&indices_buffer, 0)
            .storage_read(&vertices_buffer, 1);

        renderer.draw(&draw_command).chain_err(|| "Could not create BRDF Lut")?;
    }

    Ok(result)
}

fn discrete_floored_log2(mut n: usize) -> usize {
    let mut ret = 0;

    while n > 1 {
        n /= 2;
        ret += 1;
    }

    ret
}
