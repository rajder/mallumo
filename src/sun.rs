use super::errors::*;
use super::*;

use std::f32;
use std::f64::consts::PI;

use cgmath::prelude::Angle;
use cgmath::{Deg, Matrix4, One, Point3, Rad, Vector3, Vector4};

/// Describes sun's position with sphere coordinates.
#[derive(Copy, Clone, Debug)]
pub struct SunPosition {
    pub y_angle: Rad<f32>,
    pub height_angle: Rad<f32>,
}

impl Default for SunPosition {
    fn default() -> SunPosition {
        SunPosition {
            y_angle: Rad(0.0),
            height_angle: Rad(1.57079633),
        }
    }
}

/// Linear space vector of Sun used for ortographic projection(shadows).
#[derive(Copy, Clone, Debug)]
pub struct SunVectors {
    pub forward: Vector3<f32>,
    pub right: Vector3<f32>,
    pub up: Vector3<f32>,
}

impl SunPosition {
    // https://stackoverflow.com/a/8764866
    pub fn from_time_and_position<T: Into<Rad<f64>>>(
        year: i32,
        month: u32,
        day: u32,
        hour: u32,
        min: u32,
        sec: u32,
        latitude: T,
        longitude: T,
    ) -> SunPosition {
        let latitude = latitude.into();
        let longitude = longitude.into();

        let cumulative_days: [u32; 12] = [0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334];

        let day = day + cumulative_days[month as usize - 1];
        let leap_day = year % 4 == 0 && (year % 400 == 0 || year % 100 != 0) && day >= 60 && !(month == 2 && day == 60);
        let day = day + leap_day as u32;

        let hour = hour as f64 + min as f64 / 60.0 + sec as f64 / 3600.0;
        let delta = year - 1949;
        let leap = delta / 4;

        let jd = 32916.5 + delta as f64 * 365.0 + leap as f64 + day as f64 + hour as f64 / 24.0;

        let time = jd - 51545.0;

        let mnlong = Deg(280.460 + 0.9856474 * time).normalize();
        let mnanom = Deg(357.528 + 0.9856003 * time).normalize();

        let eclong = (mnlong + Deg(1.915 * mnanom.sin()) + Deg(0.02 * (mnanom * 2.0f64).sin())).normalize();
        let oblqec = Deg(23.439 - 0.0000004 * time);

        let num = oblqec.cos() * eclong.sin();
        let den = eclong.cos();

        let mut ra = (num / den).atan();
        if den < 0.0 {
            ra += PI;
        }
        if den >= 0.0 && num < 0.0 {
            ra += 2.0 * PI;
        }
        let ra = Rad(ra);

        let dec = (oblqec.sin() * eclong.sin()).asin();

        let mut gmst = (6.697375 + 0.0657098242 * time + hour) % 24.0;
        if gmst < 0.0 {
            gmst += 24.0;
        }

        let mut lmst = (Deg(gmst) + Deg::from(longitude) / 15.0).0 % 24.0;
        if lmst < 0.0 {
            lmst += 24.0;
        }
        let lmst = Rad::from(Deg(lmst * 15.0));

        let mut ha = (lmst - ra).0;
        if ha < -PI {
            ha += 2.0 * PI;
        } else if ha > PI {
            ha -= 2.0 * PI;
        }
        let ha = Rad(ha);

        let el = (dec.sin() * latitude.sin() + dec.cos() * latitude.cos() * ha.cos()).asin();
        let mut az = (-(dec.cos()) * ha.sin() / el.cos()).asin();

        if 0.0 < dec.sin() - el.sin() * latitude.sin() {
            if az.sin() < 0.0 {
                az += 2.0 * PI;
            }
        } else {
            az = PI - az;
        }

        SunPosition {
            y_angle: Rad(az as f32),
            height_angle: Rad(el as f32),
        }
    }

    /// Converts Sun Position to cartesian coordinates on unit sphere.
    pub fn to_cartesian(&self) -> Point3<f32> {
        let ya = self.y_angle;
        let h = self.height_angle;

        let x = Rad::sin(h) * Rad::sin(ya);
        let y = Rad::cos(h);
        let z = Rad::sin(h) * Rad::cos(ya);

        Point3::new(x, y, z)
    }

    /// Converts Sun Position to Sun Vectors for ortographic projection.
    pub fn to_vectors(&self) -> SunVectors {
        let mut forward = Vector4::new(0.0, 0.0, 1.0, 1.0);
        let mut right = Vector4::new(1.0, 0.0, 0.0, 1.0);
        let mut up = Vector4::new(0.0, 1.0, 0.0, 1.0);

        let rotation_y = Matrix4::from_angle_y(self.y_angle);
        let rotation_x = Matrix4::from_angle_x(self.height_angle);

        forward = rotation_y * rotation_x * forward;
        right = rotation_y * rotation_x * right;
        up = rotation_y * rotation_x * up;

        SunVectors {
            forward: forward.truncate(),
            right: right.truncate(),
            up: up.truncate(),
        }
    }
}

/// Holds pipelines for rendering shadowmaps and for rendering Sun(on unit sphere as cube).
pub struct SunModule {}

impl SunModule {
    /// Creates new Sun Module.
    pub fn new() -> Result<SunModule> {
        Ok(SunModule {})
    }

    /// Creates new Sun using Sun Module as pipeline holder.
    pub fn create_sun(&self, color: Vector3<f32>, position: SunPosition, shadowmap_resolution: usize) -> Result<Sun> {
        let mut shadowmap = Texture2D::new_empty(
            Texture2DSize(shadowmap_resolution, shadowmap_resolution),
            TextureInternalFormat::DepthComponent32F,
            TextureFormat::DepthComponent,
            TextureDataType::Float,
            Texture2DParameters {
                wrap_s: TextureWrapMode::ClampToBorder,
                wrap_t: TextureWrapMode::ClampToBorder,
                min: TextureTexelFilter::Nearest,
                mag: TextureTexelFilter::Nearest,
                ..Default::default()
            },
            1,
        ).chain_err(|| "Could not create shadowmap texture")?;

        let mut positions = Texture2D::new_empty(
            Texture2DSize(shadowmap_resolution, shadowmap_resolution),
            TextureInternalFormat::RGBA32F,
            TextureFormat::RGBA,
            TextureDataType::Float,
            Texture2DParameters {
                wrap_s: TextureWrapMode::Repeat,
                wrap_t: TextureWrapMode::Repeat,
                min: TextureTexelFilter::Nearest,
                mag: TextureTexelFilter::Nearest,
                ..Default::default()
            },
            1,
        ).chain_err(|| "Could not create positions texture")?;

        let mut shadowmap_framebuffer = GeneralFramebuffer::new();
        shadowmap_framebuffer.set_viewport(Viewport {
            x: 0,
            y: 0,
            width: shadowmap_resolution,
            height: shadowmap_resolution,
        });
        shadowmap_framebuffer.set_enable(EnableOption::DepthTest);
        shadowmap_framebuffer
            .attach_texture(
                FramebufferAttachment::ColorAttachment0,
                &DrawTextureAttachOption::AttachTexture(&mut positions),
            )
            .chain_err(|| "Could not attach positions texture to framebuffer")?;
        shadowmap_framebuffer
            .attach_texture(
                FramebufferAttachment::DepthAttachment,
                &DrawTextureAttachOption::AttachTexture(&mut shadowmap),
            )
            .chain_err(|| "Could not attach shadowmap to framebuffer")?;

        // shadowmap_framebuffer.set_clear_depth(1.0);
        // shadowmap_framebuffer.set_depth_test(DepthTest::Less);

        shadowmap_framebuffer.set_enable(EnableOption::CullFace);
        shadowmap_framebuffer.set_cull_face(Face::Front);

        let globals_buffer = MutableBuffer::new_empty(96).chain_err(|| "Could not create shadowmap globals buffer")?;

        let mut sun = Sun {
            color: color,
            position: SunPosition::default(),
            space_matrix: Matrix4::one(),

            globals_buffer: globals_buffer,

            shadowmap: shadowmap,
            positions: positions,
            shadowmap_framebuffer: shadowmap_framebuffer,
        };

        sun.set_position(position)
            .chain_err(|| "Could not set position of the sun")?;

        sun.set_color(color).chain_err(|| "Could not set color of the sun")?;

        Ok(sun)
    }
}

/// Holds data for use on GPU.
pub struct Sun {
    color: Vector3<f32>,
    position: SunPosition,
    space_matrix: Matrix4<f32>,

    globals_buffer: MutableBuffer,

    shadowmap: Texture2D,
    positions: Texture2D,
    shadowmap_framebuffer: GeneralFramebuffer,
}

impl Sun {
    /// Changes Sun color.
    ///
    /// Supports HDR.
    pub fn set_color(&mut self, color: Vector3<f32>) -> Result<()> {
        self.color = color;

        let color: [f32; 3] = color.into();

        self.globals_buffer
            .set_sub_data(&color, 64)
            .chain_err(|| "Could not update shadowmap globals buffer")?;

        Ok(())
    }

    pub fn set_position(&mut self, position: SunPosition) -> Result<()> {
        self.position = position;

        let radius = 3.0f32.sqrt();

        let eye = radius * -self.position.to_vectors().forward;

        //let projection = ortho(-radius, radius, -radius, radius, -2.0 * radius, 2.0 * radius);

        let near = 0.01;
        let far = 2.0 * radius;
        let reverse = Matrix4::new(
            1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, -1.0, 0.0, 0.0, 0.0, 1.0, 1.0,
        );
        let projection = Matrix4::new(
            1.0 / radius,
            0.0,
            0.0,
            0.0,
            0.0,
            1.0 / radius,
            0.0,
            0.0,
            0.0,
            0.0,
            -1.0 / (far - near),
            0.0,
            0.0,
            0.0,
            -near / (far - near),
            1.0,
        );
        let projection = reverse * projection;

        let view = Matrix4::look_at(
            Point3::new(eye.x, eye.y, eye.z),
            Point3::new(0.0, 0.0, 0.0),
            self.position.to_vectors().up,
        );

        self.space_matrix = projection * view;

        self.globals_buffer
            .set_sub_data(self.space_matrix.as_ref() as &[f32; 16], 0)
            .chain_err(|| "Could not update shadowmap globals buffer")?;

        let point: [f32; 3] = eye.into();
        self.globals_buffer
            .set_sub_data(&point, 80)
            .chain_err(|| "Could not update shadowmap globals buffer")?;

        Ok(())
    }

    /// Renders shadowmap from Sun's view.
    pub fn render_shadowmap(&mut self, renderer: &mut Renderer, shape_lists: &[&ShapeList]) -> Result<()> {
        let shadowmap_pipeline = &*fetch_program("sun/shadowmap");

        renderer.clear_framebuffer(&self.shadowmap_framebuffer, ClearBuffers::Depth);

        for shape_list in shape_lists {
            for (i, shape) in shape_list.shapes.iter().enumerate() {
                let indices = shape.indices;
                let vertices = shape.vertices;

                let primitive_parameters_buffer = &shape_list.primitive_parameters_buffer;

                let attachments = DrawTextureTarget::keep();
                let draw_command = DrawCommand::arrays(shadowmap_pipeline, 0, shape.indices.1)
                    .framebuffer(&self.shadowmap_framebuffer)
                    .attachments(&attachments)
                    .uniform(&self.globals_buffer, 0)
                    .storage_range_read::<u32>(&shape_list.indices_buffer, 0, indices.0, indices.1)
                    .storage_range_read::<Vertex>(&shape_list.vertices_buffer, 1, vertices.0, vertices.1)
                    .storage_range_read::<PrimitiveParameters>(primitive_parameters_buffer, 2, i, 1);

                renderer.draw(&draw_command).chain_err(|| "Could not draw shadowmap")?;
            }
        }

        Ok(())
    }

    pub fn get_position(&self) -> SunPosition {
        self.position
    }

    pub fn get_shadowmap(&self) -> &Texture2D {
        &self.shadowmap
    }

    pub fn get_positions(&self) -> &Texture2D {
        &self.positions
    }

    pub fn get_buffer(&self) -> &MutableBuffer {
        &self.globals_buffer
    }
}
