use super::*;

use owning_ref::MutexGuardRef;
use regex::*;
use std::collections::HashMap;
use std::str;
use std::sync::Mutex;

#[allow(dead_code)]
pub mod shaders {
    include!("../shaders.rs");
}

type ShaderProgramHM = HashMap<String, Box<Pipeline + Send + Sync>>;
type ComputeProgramHM = HashMap<String, ComputeProgram>;

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum OpenGLVendor {
    NVIDIA,
    AMD,
    Intel,
    Unknown,
}

lazy_static! {
    static ref SHADER_PROGRAMS: Mutex<ShaderProgramHM> = Mutex::new(HashMap::new());
    static ref COMPUTE_PROGRAMS: Mutex<ComputeProgramHM> = Mutex::new(HashMap::new());
    static ref INCLUDE_REGEX: Regex = Regex::new(r"(#include) (\S*)(.*)").unwrap();
    static ref ERROR_AMD_REGEX: Regex = Regex::new(r"(ERROR: \d+:)(\d+)(: )(.+)").unwrap();
    static ref ERROR_NVIDIA_REGEX: Regex = Regex::new(r"(\d+\()(\d+)(?:\) : )(.+)").unwrap();
    static ref OPENGL_VENDOR: OpenGLVendor = unsafe {
        let vendor_string = gl::GetString(gl::VENDOR);
        let vendor_string = std::ffi::CStr::from_ptr(vendor_string as *const i8).to_str().unwrap();

        if vendor_string.contains("NVIDIA") {
            OpenGLVendor::NVIDIA
        } else if vendor_string.contains("AMD") {
            OpenGLVendor::AMD
        } else if vendor_string.contains("Intel") {
            OpenGLVendor::Intel
        } else {
            OpenGLVendor::Unknown
        }
    };
}

pub fn print_errors(shader_source: &str, errors: &str) {
    let error_regex: &Regex = match *OPENGL_VENDOR {
        OpenGLVendor::NVIDIA => &*ERROR_NVIDIA_REGEX,
        _ => &*ERROR_AMD_REGEX,
    };

    let mut t = term::stdout().unwrap();

    println!("{}", errors);

    let mut error_lines: Vec<usize> = Vec::new();
    let mut error_strs: Vec<String> = Vec::new();
    for cap in error_regex.captures_iter(errors) {
        match *OPENGL_VENDOR {
            OpenGLVendor::NVIDIA => {
                error_lines.push((&cap[2]).parse::<usize>().unwrap() - 1);
                error_strs.push((&cap[3]).to_string());
            }
            _ => {
                error_lines.push((&cap[2]).parse::<usize>().unwrap() - 1);
                error_strs.push((&cap[4]).to_string());
            }
        }
    }

    for (i, line) in (&shader_source).lines().enumerate() {
        match error_lines.binary_search(&i) {
            Ok(error_index) => {
                t.fg(term::color::RED).unwrap();
                println!("{:>3}: {} // {}", i, line, error_strs[error_index]);
            }
            Err(_) => {
                t.fg(term::color::WHITE).unwrap();
                println!("{:>3}: {}", i, line);
            }
        }
    }
}

/// Replaces #include macro in shader files with corresponding file
///
/// # Arguments
/// * `shader path` - path to a shader file. Used for searching relative includes
/// * `shader source` - source code of the shader file
///
pub fn replace_includes(shader_source: &str) -> String {
    let first_pass = INCLUDE_REGEX
        .replace_all(shader_source, |caps: &Captures| {
            let filename = &caps[2];
            let filepath = "assets/shaders/".to_string() + filename;
            let file = shaders::SRC.glob(&filepath).unwrap().next();

            if let Some(file) = file {
                if let shaders::DirEntry::File(file) = file {
                    std::str::from_utf8(file.contents).unwrap().to_string()
                } else {
                    panic!("{} is not a file", file.path().display());
                }
            } else {
                panic!("File {} does not exist!", filepath);
            }
        })
        .to_string();

    INCLUDE_REGEX
        .replace_all(&first_pass, |caps: &regex::Captures| {
            let filename = &caps[2];
            let filepath = "assets/shaders/".to_string() + filename;
            let file = shaders::SRC.glob(&filepath).unwrap().next();

            if let Some(file) = file {
                if let shaders::DirEntry::File(file) = file {
                    std::str::from_utf8(file.contents).unwrap().to_string()
                } else {
                    panic!("{} is not a file", file.path().display());
                }
            } else {
                panic!("File {} does not exist!", filepath);
            }
        })
        .to_string()
}

/// Retrieves a shader program from hash map. If it does not contain one with
/// the provided name it tries to build a new one and saves it to hash map.
///
/// # Arguments
/// * `name` - name of the program
///
pub fn fetch_program<'a>(name: &str) -> MutexGuardRef<'a, ShaderProgramHM, Pipeline + Send + Sync> {
    let mut hashmap = SHADER_PROGRAMS.lock().unwrap();

    let mut found = false;
    if let Some(_) = hashmap.get(name) {
        found = true;
    }

    if found {
        return MutexGuardRef::new(hashmap).map(|mg| mg.get(name).unwrap().as_ref());
    }

    let vertex_shader_path = &("assets/shaders/".to_string() + name + ".vert");
    let geometry_shader_path = &("assets/shaders/".to_string() + name + ".geom");
    let fragment_shader_path = &("assets/shaders/".to_string() + name + ".frag");

    // Try to find .vert file
    let vertex_shader_file = shaders::SRC.glob(vertex_shader_path).unwrap().next();
    let vertex_shader: Option<Shader>;
    if let Some(vertex_shader_file) = vertex_shader_file {
        if let shaders::DirEntry::File(vertex_shader_file) = vertex_shader_file {
            let vertex_shader_source_contents = std::str::from_utf8(vertex_shader_file.contents).expect(&format!(
                "File {} is not valid UTF-8",
                vertex_shader_file.path().display()
            ));
            let vertex_shader_source = replace_includes(vertex_shader_source_contents);

            vertex_shader = Some(match Shader::new(ShaderType::Vertex, &[&vertex_shader_source]) {
                Err(e) => {
                    print_errors(&vertex_shader_source, e.description());
                    panic!("Shader {} could not be compiled", vertex_shader_path);
                }
                Ok(shader) => shader,
            });
        } else {
            panic!("{} is not a file", vertex_shader_file.path().display());
        }
    } else {
        panic!("File {} does not exist", vertex_shader_path);
    }

    // If there is corresponding geometry shader, build it and add it
    let geometry_shader_file = shaders::SRC.glob(geometry_shader_path).unwrap().next();
    let mut geometry_shader = None;
    if let Some(geometry_shader_file) = geometry_shader_file {
        if let shaders::DirEntry::File(geometry_shader_file) = geometry_shader_file {
            let geometry_shader_source_contents = std::str::from_utf8(geometry_shader_file.contents).expect(&format!(
                "File {} is not valid UTF-8",
                geometry_shader_file.path().display()
            ));
            let geometry_shader_source = replace_includes(geometry_shader_source_contents);

            geometry_shader = Some(match Shader::new(ShaderType::Geometry, &[&geometry_shader_source]) {
                Err(e) => {
                    print_errors(&geometry_shader_source, e.description());
                    panic!("Shader {} could not be compiled", geometry_shader_path);
                }
                Ok(shader) => shader,
            });
        }
    }

    // If there is corresponding fragment shader, build it and add it
    let fragment_shader_file = shaders::SRC.glob(fragment_shader_path).unwrap().next();
    let mut fragment_shader = None;
    if let Some(fragment_shader_file) = fragment_shader_file {
        if let shaders::DirEntry::File(fragment_shader_file) = fragment_shader_file {
            let fragment_shader_source_contents = std::str::from_utf8(fragment_shader_file.contents).expect(&format!(
                "File {} is not valid UTF-8",
                fragment_shader_file.path().display()
            ));
            let fragment_shader_source = replace_includes(fragment_shader_source_contents);

            fragment_shader = Some(match Shader::new(ShaderType::Fragment, &[&fragment_shader_source]) {
                Err(e) => {
                    print_errors(&fragment_shader_source, e.description());
                    panic!("Shader {} could not be compiled", fragment_shader_path);
                }
                Ok(shader) => shader,
            });
        }
    }

    let pipeline = match mallumo_gls::pipeline::pipeline::Pipeline::new(
        vertex_shader.as_ref(),
        None,
        None,
        geometry_shader.as_ref(),
        fragment_shader.as_ref(),
    ) {
        Ok(pipeline) => pipeline,
        Err(e) => {
            println!("{}", e.description());
            panic!("Unable to build {} pipeline", name);
        }
    };

    hashmap.insert(name.to_string(), Box::new(pipeline));

    return MutexGuardRef::new(hashmap).map(|mg| mg.get(name).unwrap().as_ref());
}

/// Retrieves a compute program from hash map. If it does not contain one with
/// the provided name it tries to build a new one and saves it to hash map.
///
/// # Arguments
/// * `name` - name of the program
///
pub fn fetch_compute_program<'a>(name: &str) -> MutexGuardRef<'a, ComputeProgramHM, ComputeProgram> {
    let mut hashmap = COMPUTE_PROGRAMS.lock().unwrap();

    let mut found = false;
    if let Some(_) = hashmap.get(name) {
        found = true;
    }

    if found {
        return MutexGuardRef::new(hashmap).map(|mg| mg.get(name).unwrap());
    }

    let compute_shader_path = &("assets/shaders/".to_string() + name + ".comp");
    let compute_shader_file = shaders::SRC.glob(compute_shader_path).unwrap().next();

    if let Some(compute_shader_file) = compute_shader_file {
        if let shaders::DirEntry::File(compute_shader_file) = compute_shader_file {
            let compute_shader_source_contents = std::str::from_utf8(compute_shader_file.contents).expect(&format!(
                "File {} is not valid UTF-8",
                compute_shader_file.path().display()
            ));
            let compute_shader_source = replace_includes(compute_shader_source_contents);

            // Create new compute program
            let compute_program = match ComputeProgram::new(&[&compute_shader_source]) {
                Err(e) => {
                    print_errors(&compute_shader_source, e.description());
                    panic!(
                        "Shader {} could not be compiled: \n {}",
                        compute_shader_path,
                        e.description()
                    );
                }
                Ok(compute_program) => compute_program,
            };

            // Store in in the hash map
            hashmap.insert(name.to_string(), compute_program);
        }
    } else {
        panic!("File {} does not exist", compute_shader_path);
    }

    return MutexGuardRef::new(hashmap).map(|mg| mg.get(name).unwrap());
}
