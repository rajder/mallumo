/// Builds irradiance() function in glsl.
///
/// vec3 irradiance(vec3 N, vec3 H, vec3 V, vec3 L, vec3 radiance, vec4 albedo, float roughness, float metalness)
/// where
/// N is normal vector
/// H is half vector
/// L is light vector
/// radiance is color of light
/// albedo, roughness, metalness are object pbr parameters
///
/// You can provide distribution, fresnel and geometry functions for Cook-Torrance BRDF, if not default functions are provided.
///
/// float cook_torrance_distribution(vec3 N, vec3 H, float roughness)
/// where
/// N is normal vector
/// H is half vector
/// roughness is object pbr parameter
///
/// vec3 cook_torrance_fresnel(float VdotH, vec3 f0)
/// where
/// VdotH is dot product of view and half vectors / cosine of angle between view and half vectors
/// f0 is material's response at normal incidence
///
/// float cook_torrance_geometry(vec3 N, vec3 V, vec3 L, float roughness)
/// where
/// N is normal vector
/// V is view vector
/// L is light vector
/// roughness is object pbr parameter
pub fn build_brdf(distribution: Option<&str>, fresnel: Option<&str>, geometry: Option<&str>) -> String {
    let mut result = String::from("");

    match distribution {
        Some(d) => result.push_str(d),
        None => result.push_str(include_str!("../assets/shaders/libs/cook_torrance_distribution.glsl")),
    }

    match fresnel {
        Some(f) => result.push_str(f),
        None => result.push_str(include_str!("../assets/shaders/libs/cook_torrance_fresnel.glsl")),
    }

    match geometry {
        Some(g) => result.push_str(g),
        None => result.push_str(include_str!("../assets/shaders/libs/cook_torrance_geometry.glsl")),
    }

    result.push_str(include_str!("../assets/shaders/libs/pbr.glsl"));

    result
}
