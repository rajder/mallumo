use super::errors::*;

use std::boxed::Box;
use std::path::Path;

use conrod;
use conrod::render::PrimitiveWalker;
use conrod::text::rt::*;
use conrod::Scalar;

use mallumo_gls::*;

pub const MODE_TEXT: u32 = 0;
pub const MODE_IMAGE: u32 = 1;
pub const MODE_GEOMETRY: u32 = 2;

/// Converts gamma (brightness) from sRGB to linear color space.
///
/// sRGB is the default color space for image editors, pictures, internet etc.
/// Linear gamma yields better results when doing math with colors.
pub fn gamma_srgb_to_linear(c: [f32; 4]) -> [f32; 4] {
    fn component(f: f32) -> f32 {
        // Taken from https://github.com/PistonDevelopers/graphics/src/color.rs#L42
        if f <= 0.04045 {
            f / 12.92
        } else {
            ((f + 0.055) / 1.055).powf(2.4)
        }
    }
    [component(c[0]), component(c[1]), component(c[2]), c[3]]
}

// struct Rectangle {
//     x: u32,
//     y: u32,
//     width: u32,
//     height: u32,
// }

/// The `Vertex` type passed to the vertex shader.
#[derive(Copy, Clone, Debug)]
pub struct GuiVertex {
    pub color: [f32; 4],
    pub position: [f32; 2],
    pub tex_coords: [f32; 2],
    pub mode: u32,
    pad: [u32; 3],
}

impl GuiVertex {
    fn new(pos: [f32; 2], uv: [f32; 2], color: [f32; 4], mode: u32) -> GuiVertex {
        GuiVertex {
            color: color,
            position: pos,
            tex_coords: uv,
            mode: mode,
            pad: [0, 0, 0],
        }
    }
}

#[derive(Copy, Clone, Debug)]
pub struct GuiDrawCommand {
    pub vertices: (usize, usize),

    pub texture: Option<usize>,

    pub mode: u32,
}

pub struct GuiRenderer<'a> {
    width: usize,
    height: usize,
    hidpi_factor: conrod::Scalar,

    pipeline: Box<Pipeline>,

    glyph_cache: conrod::text::GlyphCache<'a>,
    glyph_cache_texture: Texture2D,
}

impl<'a> GuiRenderer<'a> {
    pub fn new(width: usize, height: usize, hidpi_factor: f64) -> Result<GuiRenderer<'a>> {
        let vertex = Shader::from_file(ShaderType::Vertex, Path::new("assets/shaders/gui/gui.vert"))
            .chain_err(|| "Failed to compile gui vertex shader")?;
        let fragment = Shader::from_file(ShaderType::Fragment, Path::new("assets/shaders/gui/gui.frag"))
            .chain_err(|| "Failed to compile gui fragment shader")?;
        let pipeline = PipelineBuilder::new()
            .vertex_shader(&vertex)
            .fragment_shader(&fragment)
            .build()
            .chain_err(|| "Unable to build gui pipeline")?;

        let glyph_cache = conrod::text::GlyphCache::new(width as u32, height as u32, 0.1, 0.1);
        let glyph_cache_texture = Texture2D::new_empty(
            Texture2DSize(width, height),
            TextureInternalFormat::R8,
            TextureFormat::Red,
            TextureDataType::UnsignedByte,
            Texture2DParameters {
                min: TextureTexelFilter::Linear,
                mag: TextureTexelFilter::Linear,
                mipmap: TextureMipmapFilter::None,
                ..Default::default()
            },
            1,
        ).chain_err(|| "Could not create glypth texture")?;

        Ok(GuiRenderer {
            width: width,
            height: height,
            hidpi_factor: hidpi_factor,
            pipeline: Box::new(pipeline),
            glyph_cache: glyph_cache,
            glyph_cache_texture: glyph_cache_texture,
        })
    }

    pub fn render(
        &mut self,
        renderer: &mut Renderer,
        ui: &conrod::Ui,
        _image_map: &conrod::image::Map<Texture2D>,
    ) -> Result<()> {
        let old_state: FramebufferState = { renderer.get_default_framebuffer().state().clone() };

        {
            renderer.set_disable(EnableOption::DepthTest);
            renderer.set_disable(EnableOption::CullFace);
            renderer.set_enable(EnableOption::Blend);
            renderer.set_blending_equation(BlendingEquation::Addition, BlendingEquation::Addition);
            renderer.set_linear_blending_factors(
                LinearBlendingFactor::SourceAlpha,
                LinearBlendingFactor::OneMinusSourceAlpha,
                LinearBlendingFactor::SourceAlpha,
                LinearBlendingFactor::OneMinusSourceAlpha,
            );
        }

        let mut primitives = ui.draw();
        let mut vertices: Vec<GuiVertex> = Vec::new();
        let mut commands: Vec<GuiDrawCommand> = Vec::new();

        let hidpi_factor = self.hidpi_factor;

        // Framebuffer dimensions and the "dots per inch" factor.
        let (screen_w, screen_h) = (self.width, self.height);
        let (win_w, win_h) = (screen_w as Scalar, screen_h as Scalar);
        let half_win_w = win_w / 2.0;
        let half_win_h = win_h / 2.0;

        // Functions for converting for conrod scalar coords to GL vertex coords (-1.0 to 1.0).
        let vx = |x: Scalar| (x * hidpi_factor / half_win_w) as f32;
        let vy = |y: Scalar| (y * hidpi_factor / half_win_h) as f32;

        while let Some(primitive) = primitives.next_primitive() {
            let kind = primitive.kind;
            let rect = primitive.rect;

            match kind {
                conrod::render::PrimitiveKind::Rectangle { color } => {
                    let color = gamma_srgb_to_linear(color.to_fsa());
                    let (l, r, b, t) = rect.l_r_b_t();

                    let start = vertices.len();

                    {
                        let v = |x, y| {
                            // Convert from conrod Scalar range to GL range -1.0 to 1.0.
                            GuiVertex::new([vx(x), vy(y)], [0.0, 0.0], color, MODE_GEOMETRY)
                        };

                        let mut push_v = |x, y| vertices.push(v(x, y));

                        // Bottom left triangle.
                        push_v(l, t);
                        push_v(r, b);
                        push_v(l, b);

                        // Top right triangle.
                        push_v(l, t);
                        push_v(r, b);
                        push_v(r, t);
                    }

                    commands.push(GuiDrawCommand {
                        vertices: (start, 6),
                        texture: None,
                        mode: MODE_GEOMETRY,
                    });
                }
                conrod::render::PrimitiveKind::TrianglesSingleColor { color, triangles } => {
                    if triangles.is_empty() {
                        continue;
                    }

                    let color = gamma_srgb_to_linear(color.into());

                    let v = |p: [Scalar; 2]| GuiVertex::new([vx(p[0]), vy(p[1])], [0.0, 0.0], color, MODE_GEOMETRY);

                    for triangle in triangles {
                        let start = vertices.len();

                        vertices.push(v(triangle[0]));
                        vertices.push(v(triangle[1]));
                        vertices.push(v(triangle[2]));

                        commands.push(GuiDrawCommand {
                            vertices: (start, 3),
                            texture: None,
                            mode: MODE_GEOMETRY,
                        });
                    }
                }
                conrod::render::PrimitiveKind::TrianglesMultiColor { triangles } => {
                    if triangles.is_empty() {
                        continue;
                    }

                    let v = |(p, c): ([Scalar; 2], conrod::color::Rgba)| {
                        GuiVertex::new(
                            [vx(p[0]), vy(p[1])],
                            [0.0, 0.0],
                            gamma_srgb_to_linear(c.into()),
                            MODE_GEOMETRY,
                        )
                    };
                    for triangle in triangles {
                        let start = vertices.len();

                        vertices.push(v(triangle[0]));
                        vertices.push(v(triangle[1]));
                        vertices.push(v(triangle[2]));

                        commands.push(GuiDrawCommand {
                            vertices: (start, 3),
                            texture: None,
                            mode: MODE_GEOMETRY,
                        });
                    }
                }
                conrod::render::PrimitiveKind::Text { color, text, font_id } => {
                    let positioned_glyphs = text.positioned_glyphs(self.hidpi_factor as f32);

                    // Queue the glyphs to be cached
                    for glyph in positioned_glyphs {
                        self.glyph_cache.queue_glyph(font_id.index(), glyph.clone());
                    }

                    let mut unpack_alignment: i32 = 4;
                    unsafe {
                        gl::GetIntegerv(gl::UNPACK_ALIGNMENT, &mut unpack_alignment as *mut i32);
                        gl::PixelStorei(gl::UNPACK_ALIGNMENT, 1);
                    }

                    {
                        let glyph_texture = &mut self.glyph_cache_texture;
                        self.glyph_cache
                            .cache_queued(|rect, data| {
                                // TODO: investigate bug with out of bounds coordinates of conrod
                                // println!("-----------------------------------");
                                // println!("{} {}", glyph_texture.size.0, glyph_texture.size.1);
                                // println!("{} {}", rect.width() as usize, rect.height() as usize);
                                // println!("{} {}", rect.min.x as usize, rect.min.y as usize);

                                if rect.width() + rect.min.x < glyph_texture.size.0 as u32
                                    && rect.height() + rect.min.y < glyph_texture.size.1 as u32
                                {
                                    glyph_texture
                                        .set_subdata(
                                            Texture2DSize(rect.width() as usize, rect.height() as usize),
                                            Texture2DSize(rect.min.x as usize, rect.min.y as usize),
                                            &data,
                                        )
                                        .unwrap();
                                }
                            })
                            .unwrap();
                    }

                    unsafe {
                        gl::PixelStorei(gl::UNPACK_ALIGNMENT, unpack_alignment);
                    }

                    let color = gamma_srgb_to_linear(color.to_fsa());
                    let cache_id = font_id.index();

                    let origin = point(0.0, 0.0);
                    let to_gl_rect = |screen_rect: Rect<i32>| Rect {
                        min: origin
                            + (vector(
                                screen_rect.min.x as f32 / self.width as f32 - 0.5,
                                1.0 - screen_rect.min.y as f32 / self.height as f32 - 0.5,
                            )) * 2.0,
                        max: origin
                            + (vector(
                                screen_rect.max.x as f32 / self.width as f32 - 0.5,
                                1.0 - screen_rect.max.y as f32 / self.height as f32 - 0.5,
                            )) * 2.0,
                    };

                    let start = vertices.len();

                    for g in positioned_glyphs {
                        if let Ok(Some((uv_rect, screen_rect))) = self.glyph_cache.rect_for(cache_id, g) {
                            let gl_rect = to_gl_rect(screen_rect);
                            let v = |p, t| GuiVertex::new(p, t, color, MODE_TEXT);
                            let mut push_v = |p, t| vertices.push(v(p, t));
                            push_v([gl_rect.min.x, gl_rect.max.y], [uv_rect.min.x, uv_rect.max.y]);
                            push_v([gl_rect.min.x, gl_rect.min.y], [uv_rect.min.x, uv_rect.min.y]);
                            push_v([gl_rect.max.x, gl_rect.min.y], [uv_rect.max.x, uv_rect.min.y]);
                            push_v([gl_rect.max.x, gl_rect.min.y], [uv_rect.max.x, uv_rect.min.y]);
                            push_v([gl_rect.max.x, gl_rect.max.y], [uv_rect.max.x, uv_rect.max.y]);
                            push_v([gl_rect.min.x, gl_rect.max.y], [uv_rect.min.x, uv_rect.max.y]);
                        }
                    }

                    commands.push(GuiDrawCommand {
                        vertices: (start, vertices.len() - start),
                        texture: None,
                        mode: MODE_TEXT,
                    });
                }
                /*
                conrod::render::PrimitiveKind::Image {
                    image_id,
                    color,
                    source_rect,
                } => {
                    println!("Image with id {:?}", image_id);
                }
                */
                _ => {}
            }
        }

        let vertices_buffer = ImmutableBuffer::new(&vertices).chain_err(|| "Could not create vertices buffer")?;

        for command in &commands {
            let mut draw_command = match command.mode {
                MODE_TEXT => DrawCommand::arrays(self.pipeline.as_ref(), command.vertices.0, command.vertices.1)
                    .storage_read(&vertices_buffer, 0)
                    .texture_2d(&self.glyph_cache_texture, 0),
                MODE_GEOMETRY => DrawCommand::arrays(self.pipeline.as_ref(), command.vertices.0, command.vertices.1)
                    .storage_read(&vertices_buffer, 0),
                _ => DrawCommand::arrays(self.pipeline.as_ref(), 0, 3),
            };

            renderer.draw(&mut draw_command).chain_err(|| "Could not draw UI")?;
        }

        {
            renderer.get_mut_default_framebuffer().set_state(old_state);
        }

        Ok(())
    }

    pub fn resize(&mut self, width: usize, height: usize) -> Result<()> {
        self.width = width;
        self.height = height;

        self.glyph_cache = conrod::text::GlyphCache::new(width as u32, height as u32, 0.1, 0.1);
        self.glyph_cache_texture = Texture2D::new_empty(
            Texture2DSize(width, height),
            TextureInternalFormat::R8,
            TextureFormat::Red,
            TextureDataType::UnsignedByte,
            Texture2DParameters {
                min: TextureTexelFilter::Linear,
                mag: TextureTexelFilter::Linear,
                mipmap: TextureMipmapFilter::None,
                ..Default::default()
            },
            1,
        ).chain_err(|| "Could not create glypth texture")?;

        Ok(())
    }
}

// impl GuiShapeList {
//     pub fn new(ui: conrod::Ui) -> Result<GuiShapeList> {
//         Ok(GuiShapeList {

//         })
//     }
// }
