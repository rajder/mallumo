layout(std140, binding = MALLUMO_ATMOSPHERE_GLOBALS) uniform Globals 
{
  AtmosphereParameters parameters;
} globals;

float distance_to_top_atmospheric_boundary(const AtmosphereParameters atmosphere,
    float r, float mu) {
    float discriminant = max(r * r * (mu * mu - 1.0) + atmosphere.top_radius * atmosphere.top_radius, 0.0);
    return max(-r * mu + sqrt(discriminant), 0.0);
}

float unit_range_to_tex_coord(float x, int texture_size) {
    return x * (1.0 - (1.0 / texture_size)) + (0.5 / texture_size);
}

bool ray_intersects_ground(const AtmosphereParameters atmosphere,
    float r, float mu) {
    return r * r * (mu * mu - 1.0) + atmosphere.bottom_radius * atmosphere.bottom_radius >= 0.0 && mu < 0.0;
}

vec3 get_transmittance_to_top_atmospheric_boundary(
    const AtmosphereParameters atmosphere,
    sampler2D transmittance_texture,
    float r, float mu) {
    float H = sqrt(atmosphere.top_radius * atmosphere.top_radius - atmosphere.bottom_radius * atmosphere.bottom_radius);
    float rho = sqrt(max(r * r - atmosphere.bottom_radius * atmosphere.bottom_radius, 0.0));
    float d = max(-r * mu + sqrt(max((mu * mu - 1.0) * r * r + atmosphere.top_radius * atmosphere.top_radius, 0.0)), 0.0);

    float d_min = atmosphere.top_radius - r;
    float d_max = rho + H;
    float x_mu = (d - d_min) / (d_max - d_min);
    float x_r = rho / H;

    vec2 uv = vec2(
        unit_range_to_tex_coord(x_mu, TRANSMITTANCE_TEXTURE_WIDTH),
        unit_range_to_tex_coord(x_r, TRANSMITTANCE_TEXTURE_HEIGHT));

    return vec3(texture(transmittance_texture, uv));
}

float rayleigh_phase_function(float nu) {
    float k = 3.0 / (16.0 * PI);
    return k * (1.0 + nu * nu);
}

float cornette_shanks_phase_function(float g, float nu) {
    float k = 3.0 / (8.0 * PI) * (1.0 - g * g) / (2.0 + g * g);
    return k * (1.0 + nu * nu) / pow(1.0 + g * g - 2.0 * g * nu, 1.5);
}

vec3 get_transmittance_to_sun(
    const AtmosphereParameters atmosphere,
    sampler2D transmittance_texture,
    float r, float mu_s) {
    float sin_theta_h = atmosphere.bottom_radius / r;
    float cos_theta_h = -sqrt(max(1.0 - sin_theta_h * sin_theta_h, 0.0));

    return get_transmittance_to_top_atmospheric_boundary(atmosphere, transmittance_texture, r, mu_s) *
        smoothstep(-sin_theta_h * atmosphere.sun_angular_radius,
            sin_theta_h * atmosphere.sun_angular_radius,
            mu_s - cos_theta_h);
}

vec3 get_irradiance(
    const AtmosphereParameters atmosphere,
    sampler2D irradiance_texture,
    float r, float mu_s) {
    vec2 uv = vec2(
        unit_range_to_tex_coord(
            mu_s * 0.5 + 0.5, IRRADIANCE_TEXTURE_WIDTH
        ),
        unit_range_to_tex_coord(
            (r - atmosphere.bottom_radius) / (atmosphere.top_radius - atmosphere.bottom_radius), IRRADIANCE_TEXTURE_HEIGHT
        )
    );

    return vec3(texture(irradiance_texture, uv));
}

vec3 get_single_mie_scattering_from_combined_scattering(
    const AtmosphereParameters atmosphere, vec4 scattering) {
    if (scattering.r == 0.0) {
        return vec3(0.0);
    }

    return scattering.rgb * scattering.a / scattering.r *
	    (atmosphere.rayleigh_scattering.r / atmosphere.mie_scattering.r) *
	    (atmosphere.mie_scattering / atmosphere.rayleigh_scattering);
}

vec3 get_combined_scattering(
    const AtmosphereParameters atmosphere,
    sampler3D scattering_texture,
    sampler3D single_mie_scattering_texture,
    float r, float mu, float mu_s, float nu,
    bool ray_r_mu_intersects_ground,
    out vec3 single_mie_scattering) {
    float H = sqrt(atmosphere.top_radius * atmosphere.top_radius - atmosphere.bottom_radius * atmosphere.bottom_radius);
    float rho = sqrt(max(r * r - atmosphere.bottom_radius * atmosphere.bottom_radius, 0.0));
    float u_r = unit_range_to_tex_coord(rho / H, SCATTERING_TEXTURE_R_SIZE);
    float r_mu = r * mu;
    float discriminant = max(r_mu * r_mu - r * r + atmosphere.bottom_radius * atmosphere.bottom_radius, 0.0);
    float u_mu;

    if (ray_r_mu_intersects_ground) {
        float d = -r_mu - sqrt(discriminant);
        float d_min = r - atmosphere.bottom_radius;
        float d_max = rho;
        u_mu = 0.5 - 0.5 * unit_range_to_tex_coord(
            d_max == d_min ?
            0.0 :
            (d - d_min) / (d_max - d_min),
            SCATTERING_TEXTURE_MU_SIZE / 2);
    } else {
        float d = -r_mu + sqrt(discriminant + H * H);
        float d_min = atmosphere.top_radius - r;
        float d_max = rho + H;
        u_mu = 0.5 + 0.5 * unit_range_to_tex_coord((d - d_min) / (d_max - d_min), SCATTERING_TEXTURE_MU_SIZE / 2);
    }

    float d = max(
        -atmosphere.bottom_radius * mu_s +
        sqrt(
            max(
                atmosphere.bottom_radius * atmosphere.bottom_radius * (mu_s * mu_s - 1.0) + atmosphere.top_radius * atmosphere.top_radius,
                0.0
            )
        ),
    0.0);
    float d_min = atmosphere.top_radius - atmosphere.bottom_radius;
    float d_max = H;
    float a = (d - d_min) / (d_max - d_min);
    float A = -2.0 * atmosphere.mu_s_min * atmosphere.bottom_radius / (d_max - d_min);
    float u_mu_s = unit_range_to_tex_coord(max(1.0 - a / A, 0.0) / (1.0 + a), SCATTERING_TEXTURE_MU_S_SIZE);
    float u_nu = (nu + 1.0) / 2.0;

    vec4 uvwz = vec4(u_nu, u_mu_s, u_mu, u_r);
    float tex_coord_x = uvwz.x * (SCATTERING_TEXTURE_NU_SIZE - 1);
    float tex_x = floor(tex_coord_x);
    float lerp = tex_coord_x - tex_x;
    vec3 uvw0 = vec3((tex_x + uvwz.y) / SCATTERING_TEXTURE_NU_SIZE, uvwz.z, uvwz.w);
    vec3 uvw1 = vec3((tex_x + 1.0 + uvwz.y) / SCATTERING_TEXTURE_NU_SIZE, uvwz.z, uvwz.w);

    vec4 combined_scattering =
        texture(scattering_texture, uvw0) * (1.0 - lerp) +
        texture(scattering_texture, uvw1) * lerp;
    vec3 scattering = vec3(combined_scattering);
    single_mie_scattering = get_single_mie_scattering_from_combined_scattering(atmosphere, combined_scattering);

    return scattering;
}

vec3 to_earth_space(vec3 world_position, bool clamp_position) {
    vec3 clamped_world_position = clamp_position ?
        vec3(world_position.x, max(world_position.y, 0.05), world_position.z) :
        vec3(world_position.x, world_position.y, world_position.z);
    return clamped_world_position / 1000.0 + vec3(0.0, globals.parameters.bottom_radius, 0.0);
}

vec3 get_solar_luminance() {
    return globals.parameters.solar_irradiance /
        (PI * globals.parameters.sun_angular_radius * globals.parameters.sun_angular_radius) *
        globals.parameters.sun_spectral_radiance_to_luminance;
}

vec3 get_sky_luminance(
    vec3 camera_world, vec3 view_ray_world,
    vec3 sun_direction_world, out vec3 transmittance,
    sampler2D transmittance_texture, sampler3D scattering_texture) {
    vec3 camera = to_earth_space(camera_world, true).xzy;
    vec3 view_ray = view_ray_world.xzy;
    vec3 sun_direction = sun_direction_world.xzy;

    float r = length(camera);
    float rmu = dot(camera, view_ray);
    float distance_to_top_atmospheric_boundary =
        -rmu - sqrt(rmu * rmu - r * r + globals.parameters.top_radius * globals.parameters.top_radius);

    if (distance_to_top_atmospheric_boundary > 0.0) {
        camera = camera + view_ray * distance_to_top_atmospheric_boundary;
        r = globals.parameters.top_radius;
        rmu += distance_to_top_atmospheric_boundary;
    } else if (r > globals.parameters.top_radius) {
        transmittance = vec3(1.0);
        return vec3(0.0);
    }

    float mu = rmu / r;
    float mu_s = dot(camera, sun_direction) / r;
    float nu = dot(view_ray, sun_direction);
    bool ray_r_mu_intersects_ground = ray_intersects_ground(globals.parameters, r, mu);

    transmittance = ray_r_mu_intersects_ground ? vec3(0.0) :
        get_transmittance_to_top_atmospheric_boundary(
            globals.parameters, transmittance_texture, r, mu);
    vec3 single_mie_scattering;
    vec3 scattering = get_combined_scattering(
        globals.parameters, scattering_texture, scattering_texture,
        r, mu, mu_s, nu, ray_r_mu_intersects_ground,
        single_mie_scattering);

    return globals.parameters.sky_spectral_radiance_to_luminance *
        (scattering * rayleigh_phase_function(nu) + single_mie_scattering *
        cornette_shanks_phase_function(globals.parameters.mie_phase_function_g, nu));
}

vec3 get_sky_luminance_to_point(
    vec3 camera_world, vec3 point_world,
    vec3 sun_direction_world, out vec3 transmittance,
    sampler2D transmittance_texture, sampler3D scattering_texture) {
    vec3 camera = to_earth_space(camera_world, true).xzy;
    vec3 point = to_earth_space(point_world, false).xzy;
    vec3 sun_direction = sun_direction_world.xzy;

    vec3 view_ray = normalize(point - camera);
    float r = length(camera);
    float rmu = dot(camera, view_ray);
    float distance_to_top_atmospheric_boundary = -rmu - sqrt(rmu * rmu - r * r + globals.parameters.top_radius * globals.parameters.top_radius);
    if (distance_to_top_atmospheric_boundary > 0.0) {
        camera = camera + view_ray * distance_to_top_atmospheric_boundary;
        r = globals.parameters.top_radius;
        rmu += distance_to_top_atmospheric_boundary;
    }

    float mu = rmu / r;
    float mu_s = dot(camera, sun_direction) / r;
    float nu = dot(view_ray, sun_direction);
    float d = length(point - camera);
    bool ray_r_mu_intersects_ground = ray_intersects_ground(globals.parameters, r, mu);
    float r_d = clamp(sqrt(d * d + 2.0 * r * mu * d + r * r), globals.parameters.bottom_radius, globals.parameters.top_radius);
    float mu_d = clamp((r * mu + d) / r_d, -1.0, 1.0);

    if (ray_r_mu_intersects_ground) {
        transmittance = min(
            get_transmittance_to_top_atmospheric_boundary(
                globals.parameters, transmittance_texture, r_d, -mu_d) /
            get_transmittance_to_top_atmospheric_boundary(
                globals.parameters, transmittance_texture, r, -mu),
            vec3(1.0));
    } else {
        transmittance = min(
            get_transmittance_to_top_atmospheric_boundary(
                globals.parameters, transmittance_texture, r, mu) /
            get_transmittance_to_top_atmospheric_boundary(
                globals.parameters, transmittance_texture, r_d, mu_d),
            vec3(1.0));
    }

    vec3 single_mie_scattering;
    vec3 scattering = get_combined_scattering(
        globals.parameters, scattering_texture, scattering_texture,
        r, mu, mu_s, nu, ray_r_mu_intersects_ground,
        single_mie_scattering);
      
    float r_p = clamp(sqrt(d * d + 2.0 * r * mu * d + r * r), globals.parameters.bottom_radius, globals.parameters.top_radius);
    float mu_p = (r * mu + d) / r_p;
    float mu_s_p = (r * mu_s + d * nu) / r_p;
    vec3 single_mie_scattering_p;
    vec3 scattering_p = get_combined_scattering(
        globals.parameters, scattering_texture, scattering_texture,
        r_p, mu_p, mu_s_p, nu, ray_r_mu_intersects_ground,
        single_mie_scattering_p);

    scattering = scattering - transmittance * scattering_p;
    single_mie_scattering = single_mie_scattering - transmittance * single_mie_scattering_p;
    single_mie_scattering = get_single_mie_scattering_from_combined_scattering(globals.parameters, vec4(scattering, single_mie_scattering.r));
    single_mie_scattering = single_mie_scattering * smoothstep(0.0, 0.01, mu_s);
    return (scattering * rayleigh_phase_function(nu) + single_mie_scattering *
        cornette_shanks_phase_function(globals.parameters.mie_phase_function_g, nu)) *
        globals.parameters.sky_spectral_radiance_to_luminance;
}

vec3 get_sun_and_sky_illuminance(
    vec3 point_world, vec3 normal_world, vec3 sun_direction_world,
    out vec3 sky_irradiance,
    sampler2D transmittance_texture, sampler2D irradiance_texture) {
    vec3 point = to_earth_space(point_world, false).xzy;
    vec3 normal = normal_world.xzy;
    vec3 sun_direction = sun_direction_world.xzy;

    float r = length(point);
    float mu_s = dot(point, sun_direction) / r;
    sky_irradiance = get_irradiance(globals.parameters, irradiance_texture, r, mu_s) *
        (1.0 + dot(normal, point) / r) * 0.5 *
        globals.parameters.sky_spectral_radiance_to_luminance;

    return globals.parameters.solar_irradiance *
        get_transmittance_to_sun(globals.parameters, transmittance_texture, r, mu_s) *
        max(dot(normal, sun_direction), 0.0) *
        globals.parameters.sun_spectral_radiance_to_luminance;
}

float get_sun(vec3 view_direction, vec3 sun_direction) {
    float cosAngle = dot(view_direction, sun_direction);

    float g = 0.98;
    float g2 = g * g;

    return max(0.0, pow(1 - g, 2.0) / (4 * PI * pow(1.0 + g2 - 2.0 * g * cosAngle, 1.5)));
}

vec3 adjust_exposure(vec3 radiance) {
    const float ATMOSPHERIC_EXPOSURE = 0.0001;

    return radiance * ATMOSPHERIC_EXPOSURE;
}
