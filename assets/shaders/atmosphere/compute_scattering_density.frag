layout(location = 0) out vec3 scattering_density;

layout(binding = 0) uniform sampler2D transmittance_texture;
layout(binding = 1) uniform sampler3D single_rayleigh_scattering_texture;
layout(binding = 2) uniform sampler3D single_mie_scattering_texture;
layout(binding = 3) uniform sampler3D multiple_scattering_texture;
layout(binding = 4) uniform sampler2D irradiance_texture;

void main() {
    vec3 frag_coord = vec3(gl_FragCoord.xy, globals.parameters.layer + 0.5);
    float frag_coord_nu = floor(frag_coord.x / SCATTERING_TEXTURE_MU_S_SIZE);
    float frag_coord_mu_s = mod(frag_coord.x, SCATTERING_TEXTURE_MU_S_SIZE);

    vec4 uvwz = vec4(frag_coord_nu, frag_coord_mu_s, frag_coord.y, frag_coord.z) / SCATTERING_TEXTURE_SIZE;
    float H = sqrt(globals.parameters.top_radius * globals.parameters.top_radius - globals.parameters.bottom_radius * globals.parameters.bottom_radius);
    float rho = H * tex_coord_to_unit_range(uvwz.w, SCATTERING_TEXTURE_R_SIZE);
    float r = sqrt(rho * rho + globals.parameters.bottom_radius * globals.parameters.bottom_radius);
    float mu;
    bool ray_r_mu_intersects_ground;

    if (uvwz.z < 0.5) {
        float d_min = r - globals.parameters.bottom_radius;
        float d_max = rho;
        float d = d_min + (d_max - d_min) * tex_coord_to_unit_range(
            1.0 - 2.0 * uvwz.z, SCATTERING_TEXTURE_MU_SIZE / 2);
        mu = d == 0.0 ? -1.0 :
            clamp(-(rho * rho + d * d) / (2.0 * r * d), -1.0, 1.0);
        ray_r_mu_intersects_ground = true;
    } else {
        float d_min = globals.parameters.top_radius - r;
        float d_max = rho + H;
        float d = d_min + (d_max - d_min) * tex_coord_to_unit_range(
            2.0 * uvwz.z - 1.0, SCATTERING_TEXTURE_MU_SIZE / 2);
        mu = d == 0.0 ? 1.0 :
            clamp((H * H - rho * rho - d * d) / (2.0 * r * d), -1.0, 1.0);
        ray_r_mu_intersects_ground = false;
    }

    float x_mu_s = tex_coord_to_unit_range(uvwz.y, SCATTERING_TEXTURE_MU_S_SIZE);
    float d_min = globals.parameters.top_radius - globals.parameters.bottom_radius;
    float d_max = H;
    float A = -2.0 * globals.parameters.mu_s_min * globals.parameters.bottom_radius / (d_max - d_min);
    float a = (A - x_mu_s * A) / (1.0 + x_mu_s * A);
    float d = d_min + min(a, A) * (d_max - d_min);
    float mu_s = d == 0.0 ? 1.0 : clamp((H * H - d * d) / (2.0 * globals.parameters.bottom_radius * d), -1.0, 1.0);
    float nu = clamp(
        clamp(uvwz.x * 2.0 - 1.0, -1.0, 1.0),
        mu * mu_s - sqrt((1.0 - mu * mu) * (1.0 - mu_s * mu_s)),
        mu * mu_s + sqrt((1.0 - mu * mu) * (1.0 - mu_s * mu_s)));
    vec3 zenith_direction = vec3(0.0, 0.0, 1.0);
    vec3 omega = vec3(sqrt(1.0 - mu * mu), 0.0, mu);
    float sun_dir_x = omega.x == 0.0 ? 0.0 : (nu - mu * mu_s) / omega.x;
    float sun_dir_y = sqrt(max(1.0 - sun_dir_x * sun_dir_x - mu_s * mu_s, 0.0));
    vec3 omega_s = vec3(sun_dir_x, sun_dir_y, mu_s);

    const int SAMPLE_COUNT = 16;
    const float dphi = PI / SAMPLE_COUNT;
    const float dtheta = PI / SAMPLE_COUNT;
    vec3 rayleigh_mie = vec3(0.0);
    for (int l = 0; l < SAMPLE_COUNT; l++) {
        float theta = (l + 0.5) * dtheta;
        float cos_theta = cos(theta);
        float sin_theta = sin(theta);
        bool ray_r_theta_intersects_ground = ray_intersects_ground(globals.parameters, r, cos_theta);
        float distance_to_ground = 0.0;
        vec3 transmittance_to_ground = vec3(0.0);
        vec3 ground_albedo = vec3(0.0);

        if (ray_r_theta_intersects_ground) {
            distance_to_ground = max(-r * cos_theta - sqrt(max(r * r * (cos_theta * cos_theta - 1.0) + globals.parameters.bottom_radius * globals.parameters.bottom_radius, 0.0)), 0.0);
            transmittance_to_ground = get_transmittance(globals.parameters, transmittance_texture, r, cos_theta,
                distance_to_ground, true);
            ground_albedo = globals.parameters.ground_albedo;
        }

        for (int m = 0; m < 2 * SAMPLE_COUNT; m++) {
            float phi = (m + 0.5) * dphi;
            vec3 omega_i = vec3(cos(phi) * sin_theta, sin(phi) * sin_theta, cos_theta);
            float domega_i = dtheta * dphi * sin(theta);
            float nu1 = dot(omega_s, omega_i);
            vec3 incident_radiance = get_scattering(globals.parameters,
                single_rayleigh_scattering_texture, single_mie_scattering_texture,
                multiple_scattering_texture, r, omega_i.z, mu_s, nu1,
                ray_r_theta_intersects_ground, globals.parameters.scattering_order - 1);

            vec3 ground_normal = normalize(zenith_direction * r + omega_i * distance_to_ground);
            vec2 uv = vec2(
                unit_range_to_tex_coord(
                    dot(ground_normal, omega_s) * 0.5 + 0.5, IRRADIANCE_TEXTURE_WIDTH
                ),
                unit_range_to_tex_coord(
                    0.0, IRRADIANCE_TEXTURE_HEIGHT
                )
            );
            vec3 ground_irradiance =  vec3(texture(irradiance_texture, uv));
            incident_radiance += transmittance_to_ground * ground_albedo * (1.0 / PI) * ground_irradiance;
            float nu2 = dot(omega, omega_i);

            float rayleigh_density = get_density(globals.parameters.rayleigh_density, r - globals.parameters.bottom_radius);
            float mie_density = get_density(globals.parameters.mie_density, r - globals.parameters.bottom_radius);
            rayleigh_mie += incident_radiance * (
                globals.parameters.rayleigh_scattering * rayleigh_density *
                rayleigh_phase_function(nu2) +
                globals.parameters.mie_scattering * mie_density *
                cornette_shanks_phase_function(globals.parameters.mie_phase_function_g, nu2)) *
                domega_i;
        }
    }

    scattering_density = rayleigh_mie;
}
