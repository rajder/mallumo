#version 450 core

const uvec3 child_offsets[8] = {
  uvec3(0, 0, 0),
  uvec3(1, 0, 0),
  uvec3(0, 1, 0),
  uvec3(1, 1, 0),
  uvec3(0, 0, 1),
  uvec3(1, 0, 1),
  uvec3(0, 1, 1),
  uvec3(1, 1, 1)
};

// Brick pool data
layout(r32ui, binding = 0) uniform coherent volatile uimage3D brickpool;

ivec3 brick_address(int node_address)
{
    const uint brickpool_size = imageSize(brickpool).x / 3;

    return ivec3(
        3 * (node_address % brickpool_size),
        3 * ((node_address / brickpool_size) % brickpool_size),
        3 * (node_address / (brickpool_size * brickpool_size))
    );
}

void main() 
{
    ivec3 brick_address_start = brick_address(gl_VertexID);

    for(int i = 0; i < 8; i++) {
        ivec3 voxel_pos = brick_address_start + ivec3(2 * child_offsets[i]);

        vec4 voxel_albedo = unpackUnorm4x8(imageLoad(brickpool, voxel_pos).x);

        if (voxel_albedo != vec4(0.0, 0.0, 0.0, 0.0)) {
            voxel_albedo.a = 1.0;
            imageStore(brickpool, voxel_pos, uvec4(packUnorm4x8(voxel_albedo)));
        }
    }
}