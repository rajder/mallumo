layout(rgba32f, binding = 0) uniform writeonly image3D noise_texture;

const float perlin_worley_ratio = 0.3;

float dilate(float f1, float f2, float x) {
    const float curve = 0.75;
    if(x < 0.5) {
        x = x * 2.0;
        float n = f1 + f2 * x;
        return n * mix(1.0, 0.5, pow(x, curve));
    } else {
        x = (x - 0.5) * 2.0;
        float n = f2 + f1 * (1.0 - x);
        return n * mix(0.5, 1.0, pow(x, 1.0/curve));
    }
}

float change_range_to_0_1(float x, float min, float max) {
    return clamp((x - min) / (max - min), 0.0, 1.0);
}

// from: https://github.com/BrianSharpe/GPU-Noise-Lib/blob/master/gpu_noise_lib.glsl
void perlin_hash(vec3 gridcell, float s,
                    out vec4 lowz_hash_0,
                    out vec4 lowz_hash_1,
                    out vec4 lowz_hash_2,
                    out vec4 highz_hash_0,
                    out vec4 highz_hash_1,
                    out vec4 highz_hash_2)
{
    const vec2 OFFSET = vec2( 50.0, 161.0 );
    const float DOMAIN = 69.0;
    const vec3 SOMELARGEFLOATS = vec3( 635.298681, 682.357502, 668.926525 );
    const vec3 ZINC = vec3( 48.500388, 65.294118, 63.934599 );

    gridcell.xyz = gridcell.xyz - floor(gridcell.xyz * ( 1.0 / DOMAIN )) * DOMAIN;
    float d = DOMAIN - 1.5;
    vec3 gridcell_inc1 = step( gridcell, vec3( d,d,d ) ) * ( gridcell + 1.0 );

    gridcell_inc1 = mod(gridcell_inc1, s);

    vec4 P = vec4( gridcell.xy, gridcell_inc1.xy ) + OFFSET.xyxy;
    P *= P;
    P = P.xzxz * P.yyww;
    vec3 lowz_mod = vec3( 1.0 / ( SOMELARGEFLOATS.xyz + gridcell.zzz * ZINC.xyz ) );
    vec3 highz_mod = vec3( 1.0 / ( SOMELARGEFLOATS.xyz + gridcell_inc1.zzz * ZINC.xyz ) );
    lowz_hash_0 = fract( P * lowz_mod.xxxx );
    highz_hash_0 = fract( P * highz_mod.xxxx );
    lowz_hash_1 = fract( P * lowz_mod.yyyy );
    highz_hash_1 = fract( P * highz_mod.yyyy );
    lowz_hash_2 = fract( P * lowz_mod.zzzz );
    highz_hash_2 = fract( P * highz_mod.zzzz );
}


vec3 interpolation_c2(vec3 x) { 
    return x * x * x * (x * (x * 6.0 - 15.0) + 10.0); 
}

// from: https://github.com/BrianSharpe/GPU-Noise-Lib/blob/master/gpu_noise_lib.glsl
// -1.0 ... 1.0
float perlin(vec3 pos, float s) {
    pos *= s;

    vec3 Pi = floor(pos);
    vec3 Pi2 = floor(pos);
    vec3 Pf = pos - Pi;
    vec3 Pf_min1 = Pf - 1.0;

    vec4 hashx0, hashy0, hashz0, hashx1, hashy1, hashz1;
    perlin_hash( Pi2, s, hashx0, hashy0, hashz0, hashx1, hashy1, hashz1 );

    vec4 grad_x0 = hashx0 - 0.49999;
    vec4 grad_y0 = hashy0 - 0.49999;
    vec4 grad_z0 = hashz0 - 0.49999;
    vec4 grad_x1 = hashx1 - 0.49999;
    vec4 grad_y1 = hashy1 - 0.49999;
    vec4 grad_z1 = hashz1 - 0.49999;
    vec4 grad_results_0 = 1.0 / sqrt( grad_x0 * grad_x0 + grad_y0 * grad_y0 + grad_z0 * grad_z0 ) * ( vec2( Pf.x, Pf_min1.x ).xyxy * grad_x0 + vec2( Pf.y, Pf_min1.y ).xxyy * grad_y0 + Pf.zzzz * grad_z0 );
    vec4 grad_results_1 = 1.0 / sqrt( grad_x1 * grad_x1 + grad_y1 * grad_y1 + grad_z1 * grad_z1 ) * ( vec2( Pf.x, Pf_min1.x ).xyxy * grad_x1 + vec2( Pf.y, Pf_min1.y ).xxyy * grad_y1 + Pf_min1.zzzz * grad_z1 );

    vec3 blend = interpolation_c2( Pf );
    vec4 res0 = mix( grad_results_0, grad_results_1, blend.z );
    vec4 blend2 = vec4( blend.xy, vec2( 1.0 - blend.xy ) );
    float final = dot( res0, blend2.zxzx * blend2.wwyy );
    final *= 1.0/sqrt(0.75);
    return ((final * 1.5) + 1.0) * 0.5;
}

float perlin_3D_7_octaves(vec3 pos, float s) {
    float f = 1.0;
    float a = 1.0;

    float perlin_value = 0.0;
    perlin_value += a * perlin(pos, s * f); a *= 0.5; f *= 2.0;
    perlin_value += a * perlin(pos, s * f); a *= 0.5; f *= 2.0;
    perlin_value += a * perlin(pos, s * f); a *= 0.5; f *= 2.0;
    perlin_value += a * perlin(pos, s * f); a *= 0.5; f *= 2.0;
    perlin_value += a * perlin(pos, s * f); a *= 0.5; f *= 2.0;
    perlin_value += a * perlin(pos, s * f); a *= 0.5; f *= 2.0;
    perlin_value += a * perlin(pos, s * f);

    return perlin_value;
}

vec3 worley_3D_hash( vec3 x, float s) {
    x = mod(x, s);
    x = vec3( dot(x, vec3(127.1,311.7, 74.7)),
              dot(x, vec3(269.5,183.3,246.1)),
              dot(x, vec3(113.5,271.9,124.6)));
    
    return fract(sin(x) * 43758.5453123);
}

vec3 worley_3D(vec3 x, float s, bool inverted) {
    x *= s;
    x += 0.5;
    vec3 p = floor(x);
    vec3 f = fract(x);

    float id = 0.0;
    vec2 res = vec2( 1.0 , 1.0);
    for(int k=-1; k<=1; k++){
        for(int j=-1; j<=1; j++) {
            for(int i=-1; i<=1; i++) {
                vec3 b = vec3(i, j, k);
                vec3 r = vec3(b) - f + worley_3D_hash(p + b, s);
                float d = dot(r, r);

                if(d < res.x) {
                    id = dot(p + b, vec3(1.0, 57.0, 113.0));
                    res = vec2(d, res.x);			
                } else if(d < res.y) {
                    res.y = d;
                }
            }
        }
    }

    vec2 result = res;
    id = abs(id);

    if(inverted)
        return vec3(1.0 - result, id);
    else
        return vec3(result, id);
}

float worley_3D_3_octaves(vec3 pos, float s)
{
    float w1 = worley_3D(pos, 1.0 * s, true).r;
    float w2 = worley_3D(pos, 2.0 * s, false).r;
    float w3 = worley_3D(pos, 4.0 * s, false).r;

    return clamp(w1 - 0.3 * w2 - 0.3 * w3, -1000.0, 10000.0);
}

void main()
{
    int noise_size = imageSize(noise_texture).r;

    int x = gl_VertexID % noise_size;
    int y = (gl_VertexID / noise_size) % noise_size;
    int z = (gl_VertexID / (noise_size * noise_size)) % noise_size;

    vec3 pos = vec3(
        x / float(noise_size),
        y / float(noise_size),
        z / float(noise_size));

    float p = change_range_to_0_1(perlin_3D_7_octaves(pos, 4.0), 0.58, 1.5);

    const float worley_scale = 7.0;
    float w1 = change_range_to_0_1(worley_3D_3_octaves(pos, worley_scale), -0.1, 1.0);
    float w2 = change_range_to_0_1(worley_3D_3_octaves(pos, worley_scale * 2), -0.1, 1.0);
    float w3 = change_range_to_0_1(worley_3D_3_octaves(pos, worley_scale * 4), -0.1, 1.0);

    imageStore(noise_texture, ivec3(x, y, z), vec4(dilate(p, w1, perlin_worley_ratio), w1, w2, w3));
}
