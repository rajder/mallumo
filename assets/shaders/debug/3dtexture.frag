#version 450 core

layout (location = 0) out vec4 color;

in geometryOut {
  flat uvec3 voxel_position;
} geometry_out;

layout(std140, binding = 0) uniform Globals 
{
    uint size;
} globals;

layout(binding = 0) uniform sampler3D voxel_texture;

void main()
{
    vec4 tex_color = texelFetch(voxel_texture, ivec3(geometry_out.voxel_position), 0);

    if (tex_color.a == 0) {
      discard;
    }

    color = vec4(tex_color.rgb, 1.0);
}