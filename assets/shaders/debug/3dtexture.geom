#version 450 core

layout (points) in;
layout (triangle_strip, max_vertices=14) out;

layout(std140, binding = 0) uniform Globals 
{
    uint size;
} globals;

layout(std140, binding = 1) uniform Camera 
{
  mat4 projection_matrix;
  mat4 view_matrix;
  mat4 projection_view_matrix;
  vec4 camera_position;
} camera;

in gl_PerVertex
{
  vec4 gl_Position;
} gl_in[];

in vertexOut
{
  uvec3 voxel_position;
} vertex_out[];

out geometryOut {
  flat uvec3 voxel_position;
} geometry_out;

const vec3 triangle_offsets[14] = {
  vec3(-1.0, 1.0, -1.0),
  vec3(1.0, 1.0, -1.0),
  vec3(-1.0, -1.0, -1.0),
  vec3(1.0, -1.0, -1.0),
  vec3(1.0, -1.0, 1.0),
  vec3(1.0, 1.0, -1.0),
  vec3(1.0, 1.0, 1.0),
  vec3(-1.0, 1.0, -1.0),
  vec3(-1.0, 1.0, 1.0),
  vec3(-1.0, -1.0, -1.0),
  vec3(-1.0, -1.0, 1.0),
  vec3(1.0, -1.0, 1.0),
  vec3(-1.0, 1.0, 1.0),
  vec3(1.0, 1.0, 1.0),
};

void main()
{
    for(int i = 0; i < triangle_offsets.length(); i++) {
      geometry_out.voxel_position = vertex_out[0].voxel_position;
      gl_Position = camera.projection_view_matrix * (gl_in[0].gl_Position + vec4(triangle_offsets[i] * (1.0 / float(globals.size)), 1.0));
      EmitVertex();
    }
    EndPrimitive();
}  