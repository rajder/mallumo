#version 450 core

in VertexOut {
    vec3 world_position;
    vec3 world_normal;
    vec2 texture_coordinate;
    mat3 TBN;

    flat int draw_id;
} fragment_in;

layout(std140, binding = 0) uniform Globals 
{
    uint size;
} globals;

layout(rgba8, binding = 0) uniform coherent writeonly image3D voxel_texture;

layout ( binding = 0, offset = 0 ) uniform atomic_uint voxels_count;

void main() {
    atomicCounterIncrement(voxels_count);

    imageStore(voxel_texture, ivec3(0, 0, 0), vec4(0.5, 0.75, 0, 0));
}