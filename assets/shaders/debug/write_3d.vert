#version 450

layout(r32ui, binding = 0) uniform coherent volatile uimage3D first;
layout(r32ui, binding = 1) uniform coherent volatile uimage3D second;

void main() {
    int size = imageSize(first).x;

    ivec3 brick_address = ivec3(
        gl_VertexID % size,
        (gl_VertexID / size) % size,
        (gl_VertexID / size / size) % size
    );

    uint position = packUnorm4x8(vec4(vec3(brick_address) / float(size), 1.0));

    imageStore(first, brick_address, uvec4(position, 0.0, 0.0, 0.0));
    imageStore(second, brick_address, uvec4(position, 0.0, 0.0, 0.0));

    memoryBarrier();
}