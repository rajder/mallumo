// version
// positions
// vertices
// parameters

layout(std140, binding = 0) uniform Camera 
{
  mat4 projection_matrix;
  mat4 view_matrix;
  mat4 projection_view_matrix;
  vec4 position;
} camera;

out vec3 world_position;

void main(void)
{
  int index = indices[gl_VertexID];

  vec3 position = vertices[index].position.xyz;

  gl_Position = vec4(position, 1.0f);
}