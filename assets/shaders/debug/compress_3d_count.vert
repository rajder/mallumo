
#version 450

layout(binding = 0) uniform sampler3D voxel_texture;

layout (binding = 1, offset = 0) uniform atomic_uint voxels_count;

void main() {
  uint voxel_size = uint(textureSize(voxel_texture, 0).x);

  uint x = gl_VertexID % voxel_size;
  uint y = (gl_VertexID / voxel_size) % voxel_size;
  uint z = gl_VertexID / (voxel_size * voxel_size);

  vec4 voxel_color = texelFetch(voxel_texture, ivec3(x, y, z), 0);

  if(voxel_color.a != vec4(0.0, 0.0, 0.0, 0.0)) {
    uint index = atomicCounterIncrement(voxels_count);
  }
}
