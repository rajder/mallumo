#version 450 core

const vec2 invAtan = vec2(0.1591, 0.3183);
vec2 SampleSphericalMap(vec3 v)
{
    vec2 uv = vec2(atan(v.z, v.x), asin(v.y));
    uv *= invAtan;
    uv += 0.5;
    return uv;
}

layout(std140, binding = 0) uniform Camera
{
    mat4 projection;
    mat4 view;
} camera;

in vertexOut {
    vec3 position;
} vertex_out;

layout(binding = 0) uniform sampler2D equirectangular_sampler;

layout (location = 0) out vec4 color;

void main(void)
{
    vec2 uv = SampleSphericalMap(normalize(vertex_out.position));

    color = vec4(texture(equirectangular_sampler, vec2(uv.x, 1.0 - uv.y)).rgb, 1.0);
}