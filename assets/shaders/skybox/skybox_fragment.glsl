#version 450 core

in vertexOut {
    vec3 position;
} vertex_out;

layout(binding = 0) uniform samplerCubeArray cubemap_sampler;

layout(location = 0) out vec4 color;
  
void main()
{
    vec3 Lo = texture(cubemap_sampler, vec4(vertex_out.position, 0)).rgb;

    // HDR -> LDR using Reinhard operator
    float min_white = 1.5;
    Lo = (Lo * (vec3(1.0) + Lo / vec3(min_white * min_white))) / (vec3(1.0) + Lo);
    
    // Gamma correction
    Lo = pow(Lo, vec3(1.0/2.2));
  
    color = vec4(Lo, 1.0) / 1.7;
}