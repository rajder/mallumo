#version 450 core

layout(std140, binding = 0) uniform Camera
{
    mat4 projection;
    mat4 view;
} camera;

layout(std140, binding = 1) uniform Globals
{
    float roughness;
} globals;

layout(std430, binding = 0) buffer Indices
{
    uint indices[];
} indices;

layout(std430, binding = 1) buffer VertexAttributes
{
    vec4 va[];
} vertexAttributes;

out vertexOut {
    vec3 position;
} vertex_out;

void main(void)
{
  uint index = indices.indices[gl_VertexID];
  vec3 position = vertexAttributes.va[index].xyz;

  vertex_out.position = position;

  gl_Position = camera.projection * camera.view * vec4(position, 1.0f);
}