const float gaussianWeight[4] = {0.25, 0.125, 0.0625, 0.03125};

uint[8] load_child_tile(in int tile_address) {
  uint nodes[8];

  for (int i = 0; i < 8; ++i) {
    nodes[i] = nodepool_next[tile_address + i];
  }
  memoryBarrier();

  return nodes;
}

vec4 get_color(in uint nodes[8], in ivec3 position) {
  ivec3 child_position = ivec3(round(vec3(position) / 4.0));
  int child_index = child_position.x + 2 * child_position.y + 4 * child_position.z;

  ivec3 local_position = position - 2 * child_position;

  ivec3 child_brick_address = ivec3(tiles[childIndex] * 3, 0, 0);

  return imageLoad(brickpool_value, child_brick_address + local_position);
}

vec4 mipmap_isotropic(in uint nodes[8], in ivec3 pos) {
  vec4 col = vec4(0);
  float weightSum = 0.0;
  
  for (int x = -1; x <= 1; ++x) {
    for (int y = -1; y <= 1; ++y) {
      for (int z = -1; z <= 1; ++z) {
        
        const ivec3 lookupPos = pos + ivec3(x, y, z);

        if (lookupPos.x >= 0 && lookupPos.y >= 0 && lookupPos.z >= 0 &&
            lookupPos.x <= 4 && lookupPos.y <= 4 && lookupPos.z <= 4)
        {
          const int manhattanDist = abs(x) + abs(y) + abs(z);
          const float weight = gaussianWeight[manhattanDist];
          const vec4 lookupColor = get_color(nodes, lookupPos);

          col += weight * lookupColor;
          weightSum += weight;
        }

      }
    }
  }

  return col / weightSum;
}

