///
/// Arguments
/// * nodepool_next - buffer storing nodes with pointers to children
/// * levels - number of levels that should be searched (depth)
/// * voxel_position - voxel position in scene
///
/// Out
/// * found_on_level - on which level the node with no children was found
///
/// Result
///   Address of a node where the voxel belongs
int traverse_octree(out 
                    in uint levels, 
                    in vec3 voxel_position, 
                    out uint found_on_level) 
{
  vec3 node_position = vec3(0.0);
  vec3 node_position_max = vec3(1.0);

  int node_address = 0;
  found_on_level = 0;
  float side_length = 1.0;
  
  for (uint level = 0; level < levels; ++level) {
    uint node = imageLoad(nodepool_next, node_address).x;

    // if current node points to *null* (0) nodepool
    uint child_start_address = node & NODE_MASK_VALUE;
    if (child_start_address == 0U) {
        found_on_level = level;
        break;
    }
    
    uvec3 offset_vec = uvec3(2.0 * voxel_position);
    uint offset = offset_vec.x + 2U * offset_vec.y + 4U * offset_vec.z;

    node_address = int(child_start_address + offset);
    node_position += vec3(child_offsets[offset]) * vec3(side_length);
    node_position_max = node_position + vec3(side_length);

    side_length = side_length / 2.0;
    voxel_position = 2.0 * voxel_position - vec3(offset_vec);
  } 

  return node_address;
}
