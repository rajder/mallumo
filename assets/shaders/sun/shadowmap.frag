#version 450 core

in vec3 world_position;

layout(location = 0) out vec3 position;

void main() {
  position = world_position;
  gl_FragDepth = gl_FragCoord.z;
}