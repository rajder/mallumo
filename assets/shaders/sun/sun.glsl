layout(std140, binding = SUN_BINDING) uniform Sun {
  mat4 space_matrix;
  vec4 diffuse;
  vec4 position;
} sun;

layout(binding = SUN_SHADOWMAP_BINDING) uniform sampler2D sun_shadowmap;

float in_sun_shadow(vec4 sun_space_position, vec3 normal)
{
    // perform perspective divide
    vec3 projection_coords = sun_space_position.xyz / sun_space_position.w;

    // transform to [0,1] range
    projection_coords.xy = projection_coords.xy * 0.5 + 0.5;

    // get closest depth value from light's perspective (using [0,1] range fragPosLight as coords)
    float closest_depth = texture(sun_shadowmap, projection_coords.xy).r; 

    // get depth of current fragment from light's perspective
    float current_depth = projection_coords.z;

    float depth = texture(sun_shadowmap, projection_coords.xy).r;

    return current_depth < depth ? 1.0 : 0.0;
}

float in_sun_shadow_pcf(vec4 sun_space_position, vec3 normal)
{
    // perform perspective divide
    vec3 projection_coords = sun_space_position.xyz / sun_space_position.w;

    // transform to [0,1] range
    projection_coords.xy = projection_coords.xy * 0.5 + 0.5;

    // get closest depth value from light's perspective (using [0,1] range fragPosLight as coords)
    float closest_depth = texture(sun_shadowmap, projection_coords.xy).r; 

    // get depth of current fragment from light's perspective
    float current_depth = projection_coords.z;

    float shadow = 0.0;
    vec2 texel_size = 1.0 / textureSize(sun_shadowmap, 0);
    for(int x = -6; x <= 6; ++x)
    {
        for(int y = -6; y <= 6; ++y)
        {
            float depth = texture(sun_shadowmap, projection_coords.xy + vec2(x, y) * texel_size).r;
            shadow += current_depth < depth ? 1.0 : 0.0;
        }    
    }
    shadow /= 13.0 * 13.0;

    return shadow;
}