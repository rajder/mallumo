#version 450 core

#define VXGI_OPTIONS_BINDING 0
#include vxgi/options.glsl

#define CAMERA_BINDING 1
#include libs/camera.glsl

layout(binding = 0) buffer ConePath {
  vec4 cone_path[256];
  uint cone_count;
};

layout(binding = 0) uniform sampler2D position_texture;
layout(binding = 1) uniform sampler2D normal_texture;
layout(binding = 2) uniform sampler3D volume_radiance[10];

layout(location = 0) uniform vec2 screen_coordinates;

vec4 vxgi_sample_function(vec3 position, float lod) {
  lod = clamp(lod, 0.0f, float(vxgi_options.levels));

  int base_lod = int(floor(lod));
  int mip_lod = int(ceil(lod));

  vec4 base_value = vec4(0.0);
  vec4 mip_value = vec4(0.0);

  base_value = texture(volume_radiance[base_lod], position);
  mip_value = texture(volume_radiance[mip_lod], position);

  // Alpha correct
  // const float alpha_correction = float(pow2[base_lod + 1]) / float(pow2[vxgi_options.levels]);

  // correct_alpha(base_value, alpha_correction);
  // correct_alpha(mip_value, alpha_correction * 2);

  vec4 result = mix(base_value, mip_value, lod - base_lod);

  return result;
}

bool intersect_ray_with_unit_aabb(in vec3 origin, in vec3 direction, out float t_enter, out float t_leave) {
  vec3 temp_min = (-origin) / direction;
  vec3 temp_max = (vec3(1.0) - origin) / direction;

  vec3 v3_max = max(temp_max, temp_min);
  vec3 v3_min = min(temp_max, temp_min);

  t_leave = min(v3_max.x, min(v3_max.y, v3_max.z));
  t_enter = max(max(v3_min.x, 0.0), max(v3_min.y, v3_min.z));

  return t_leave > t_enter;
}

void trace_cone(vec3 position, vec3 normal, vec3 direction, float aperture) {
  float max_distance = 1.0;

  // Convert position from world space to texture space
  position = position * 0.5 + 0.5;

  float voxel_size = 1.0 / float(vxgi_options.dimension);

  // move further to avoid self collision
  float t = 2.0 * voxel_size;

  // final results
  vec4 cone_sample = vec4(0.0f);

  // out of boundaries check
  float enter = 0.0;
  float leave = 0.0;

  // if (!intersect_ray_with_unit_aabb(position, direction, enter, leave)) {
  //   cone_sample.a = 1.0f;
  // }

  int count = 0;
  while (cone_sample.a < 1.0f && t <= max_distance) {
    vec3 cone_position = position + direction * t;

    if (any(greaterThan(cone_position, vec3(1.0))) || any(lessThan(cone_position, vec3(0.0)))) {
      break;
    }

    if (count > 4) {
      break;
    }

    // cone expansion and respective mip level based on diameter
    float diameter = 2.0f * tan(aperture) * t;
    float mip_level = log2(diameter / voxel_size);

    cone_path[count] = vec4(cone_position, mip_level);

    // sample voxel structure at current position and mip level of cone
    vec4 current_sample = vxgi_sample_function(cone_position, mip_level);

    // front to back composition
    cone_sample += (1.0f - cone_sample.a) * current_sample;

    // move further into volume
    if (vxgi_options.anisotropic) {
      t += diameter;
    } else {
      t += diameter / 3.0;
    }

    count++;
  }
  cone_count = count;
}

void main() {
  vec2 uv = screen_coordinates;
  uv.y = 1.0 - uv.y;

  vec3 position = texture(position_texture, uv).xyz;
  vec3 normal = texture(normal_texture, uv).xyz;

  // vec3 guide = vec3(0.0f, 1.0f, 0.0f);

  // if (abs(dot(normal, guide)) == 1.0f) {
  //   guide = vec3(0.0f, 0.0f, 1.0f);
  // }

  // // Find a tangent and a bitangent
  // const vec3 right = normalize(guide - dot(normal, guide) * normal);
  // const vec3 up = cross(right, normal);

  trace_cone(position, normal, normal, vxgi_options.cones[0].w);
}