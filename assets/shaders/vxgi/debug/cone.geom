#version 450 core

#define VXGI_OPTIONS_BINDING 0
#define CAMERA_BINDING 1

#include vxgi/options.glsl
#include libs/camera.glsl

layout (points) in;
layout (triangle_strip, max_vertices=14) out;

in gl_PerVertex
{
  vec4 gl_Position;
} gl_in[];


in vec3 position[];
in float mip_level[];

const vec3 triangle_offsets[14] = {
  vec3(-1.0, 1.0, -1.0),
  vec3(1.0, 1.0, -1.0),
  vec3(-1.0, -1.0, -1.0),
  vec3(1.0, -1.0, -1.0),
  vec3(1.0, -1.0, 1.0),
  vec3(1.0, 1.0, -1.0),
  vec3(1.0, 1.0, 1.0),
  vec3(-1.0, 1.0, -1.0),
  vec3(-1.0, 1.0, 1.0),
  vec3(-1.0, -1.0, -1.0),
  vec3(-1.0, -1.0, 1.0),
  vec3(1.0, -1.0, 1.0),
  vec3(-1.0, 1.0, 1.0),
  vec3(1.0, 1.0, 1.0),
};

void main()
{
    vec3 position = gl_in[0].gl_Position.xyz * 2.0 - 1.0;
    float edge_size = (1.0 / float(vxgi_options.dimension)) * pow(2.0, mip_level[0]);

    for(int i = 0; i < triangle_offsets.length(); i++) {
      gl_Position = camera.projection_view_matrix * vec4(position + triangle_offsets[i] * (edge_size / 3.0), 1.0);
      EmitVertex();
    }

    EndPrimitive();
}  