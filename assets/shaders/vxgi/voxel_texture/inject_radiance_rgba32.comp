#version 450 core

layout (local_size_x = 8, local_size_y = 8, local_size_z = 8) in;

// Uniform bindings
#define VXGI_OPTIONS_BINDING 0
#define SUN_BINDING 1

// Sampler bindings
#define VOXEL_ALBEDO_BINDING 0
#define VOXEL_NORMAL_BINDING 1
#define VOXEL_EMISSION_BINDING 2
#define SUN_SHADOWMAP_BINDING 3

#include vxgi/options.glsl
#include sun/sun.glsl

layout (binding = VOXEL_ALBEDO_BINDING)   uniform sampler3D voxel_albedo;
layout (binding = VOXEL_NORMAL_BINDING)   uniform sampler3D voxel_normal;
layout (binding = VOXEL_EMISSION_BINDING) uniform sampler3D voxel_emission;

layout (binding = 0, rgba32f) uniform image3D voxel_radiance;

const float PI = 3.14159265f;
const float EPSILON = 1e-30;

vec3 encode_normal(vec3 normal)
{
    return normal * 0.5f + vec3(0.5f);
}

vec3 decode_normal(vec3 normal)
{
    return normal * 2.0f - vec3(1.0f);
}

vec4 direct_lighting(vec3 position, vec3 normal, vec3 albedo)
{
    float voxel_size = 2.0 / float(vxgi_options.dimension);
    position = position + normal * voxel_size; // move position forward to avoid shadowing errors

    float visibility = 1.0 - in_sun_shadow(sun.space_matrix * vec4(position, 1.0), normal);
    vec3 light_direction = normalize(sun.position.xyz);

    vec3 weight = normal * normal;

    // calculate directional normal attenuation
    float XdotL = dot(vec3(1.0, 0.0, 0.0), light_direction);
    float YdotL = dot(vec3(0.0, 1.0, 0.0), light_direction);
    float ZdotL = dot(vec3(0.0, 0.0, 1.0), light_direction);

    XdotL = normal.x > 0.0 ? max(XdotL, 0.0) : max(-XdotL, 0.0);
    YdotL = normal.y > 0.0 ? max(YdotL, 0.0) : max(-YdotL, 0.0);
    ZdotL = normal.z > 0.0 ? max(ZdotL, 0.0) : max(-ZdotL, 0.0);

    // voxel shading average from all front sides
    float NdotL = XdotL * weight.x + YdotL * weight.y + ZdotL * weight.z;

    vec3 diffuse = vec3(sun.diffuse);
    vec3 radiance = diffuse * albedo * NdotL * visibility;

    return vec4(radiance, visibility);
}

void main()
{
	if(gl_GlobalInvocationID.x >= vxgi_options.dimension ||
		gl_GlobalInvocationID.y >= vxgi_options.dimension ||
		gl_GlobalInvocationID.z >= vxgi_options.dimension) { return; }

	ivec3 write_position = ivec3(gl_GlobalInvocationID);

    // Voxel albedo
	vec4 albedo = texelFetch(voxel_albedo, write_position, 0);
    
    if(albedo.a < EPSILON) { 
        return; 
    }

    // Voxel normal
    vec3 normal = texelFetch(voxel_normal, write_position, 0).xyz;
    normal = decode_normal(normal);
    normal = normalize(normal);

    // Voxel emission
    vec3 emission = texelFetch(voxel_emission, write_position, 0).rgb;

    // black voxel has no irradiance diffuse
    if(any(greaterThan(albedo.rgb, vec3(0.0f))))
    {
        // obtain world-space position of the current voxel
        vec3 position = vec3(write_position) / float(vxgi_options.dimension);
        vec3 position_ws = position * 2.0 - 1.0;

        // calculate direct lighting onto voxel
        albedo = direct_lighting(position_ws, normal, albedo.rgb);

        imageStore(voxel_radiance, write_position, vec4(albedo.rgb, 1.0));
    }
}