#version 450 core

#define VXGI_OPTIONS_BINDING 0
#define CAMERA_BINDING 1
#define SUN_BINDING 2
#define SUN_SHADOWMAP_BINDING 5

#include libs/consts.glsl
#include libs/cook_torrance_fresnel.glsl
#include libs/cook_torrance_distribution.glsl
#include libs/cook_torrance_geometry.glsl
#include libs/pbr.glsl
#include libs/camera.glsl
#include sun/sun.glsl
#include vxgi/options.glsl

// Volume textures
layout(binding = 6) uniform sampler3D volume_radiance_base;
layout(binding = 7) uniform sampler3D volume_radiance_mip[6];
layout(binding = 13) uniform sampler3D volume_emission_base;
layout(binding = 14) uniform sampler3D volume_emission_mip[6];

vec4 vxgi_sample_function(vec3 position, vec3 weight, uvec3 face, float lod, VoxelTextureType type)
{
    face.x = clamp(face.x, 0U, 1U);
    face.y = clamp(face.y, 2U, 3U);
    face.z = clamp(face.z, 4U, 5U);

    float anisotropic_level = max(lod - 1.0f, 0.0f);
    vec4  anisotropic_sample = vec4(0.0f);
    if (type == VoxelTextureTypeRadiance) {
        anisotropic_sample = weight.x * textureLod(volume_radiance_mip[face.x], position, anisotropic_level)
                           + weight.y * textureLod(volume_radiance_mip[face.y], position, anisotropic_level)
                           + weight.z * textureLod(volume_radiance_mip[face.z], position, anisotropic_level);
    } 
    else if (type == VoxelTextureTypeEmission) {
        anisotropic_sample = weight.x * textureLod(volume_emission_mip[face.x], position, anisotropic_level)
                           + weight.y * textureLod(volume_emission_mip[face.y], position, anisotropic_level)
                           + weight.z * textureLod(volume_emission_mip[face.z], position, anisotropic_level);
    }

    // linearly interpolate on base level
    if(lod < 1.0f)
    {
        vec4 base_sample = vec4(0.0);
        if (type == VoxelTextureTypeRadiance) {
            base_sample = texture(volume_radiance_base, position);
        } 
        else if (type == VoxelTextureTypeEmission) {
            base_sample = texture(volume_emission_base, position);
        }

        anisotropic_sample = mix(base_sample, anisotropic_sample, clamp(lod, 0.0f, 1.0f));
    }

    return anisotropic_sample;
}

#include vxgi/gi_shared.glsl