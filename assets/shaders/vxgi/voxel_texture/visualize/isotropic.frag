#version 450 core

const uint VoxelTextureTypeAlbedo = 0;
const uint VoxelTextureTypeNormal = 1;
const uint VoxelTextureTypeEmission = 2;
const uint VoxelTextureTypeMetallicRoughness = 3;
const uint VoxelTextureTypeRadiance = 4;

layout(location = 0) uniform uint dimension;
layout(location = 1) uniform uint type;

layout (location = 0) out vec4 color;

layout(location = 0) in flat vec4 geometry_voxel_color;

void main()
{
    if (geometry_voxel_color.a < 0.0001) {
        discard;
    }

    color = vec4(geometry_voxel_color.rgb, 1.0);
}