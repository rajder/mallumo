#version 450 core

#define VXGI_OPTIONS_BINDING 0
#define SUN_BINDING 1
#define SUN_SHADOWMAP_BINDING 0
#define SUN_POSITIONS_BINDING 1

#include sun/sun.glsl
#include vxgi/options.glsl
#include vxgi/svo/voxelize/shared.glsl

layout(binding = SUN_POSITIONS_BINDING) uniform sampler2D sun_positions;

/// Sparse Voxel Octagonal Tree data
// Node pool data
layout(std430, binding = 0) buffer NodePoolNext
{
  uint nodepool_next[];
};

// Brick pool data
layout(rgba8, binding = 0) uniform coherent volatile image3D brickpool_albedos;
layout(rgba8, binding = 1) uniform coherent volatile image3D brickpool_irradiances;

void main() {
    const int sun_positions_size = textureSize(sun_positions, 0).x;

    vec2 uv = vec2(0);
    uv.x = (gl_VertexID % sun_positions_size) / float(sun_positions_size);
    uv.y = (gl_VertexID / sun_positions_size) / float(sun_positions_size);

    vec3 voxel_position = texture(sun_positions, uv).xyz * 0.5 + 0.5;

    vec3 node_position_min = vec3(0.0);
    vec3 node_position_max = vec3(1.0);

    int node_address = 0;
    float side_length = 1.0;
    
    for (uint level = 0; level < vxgi_options.levels; ++level) {
        uint node = nodepool_next[node_address] & NODE_MASK_VALUE;

        // if current node points to *null* (0) nodepool
        uint child_start_address = node & NODE_MASK_VALUE;
        if (child_start_address == 0U) {
            // Calculate brick position
            const uint brickpool_size = imageSize(brickpool_albedos).x / 3;
            ivec3 brick_coordinates = ivec3(
                3 * (node_address % brickpool_size),
                3 * ((node_address / brickpool_size) % brickpool_size),
                3 * ((node_address / brickpool_size / brickpool_size) % brickpool_size)
            );

            uvec3 offset_vec = uvec3(2.0 * voxel_position);
            uint offset = offset_vec.x + 2U * offset_vec.y + 4U * offset_vec.z;
            ivec3 injection_pos = brick_coordinates  + 2 * ivec3(child_offsets[offset]);

            // Calculate irradiance
            vec4 voxel_albedo = imageLoad(brickpool_albedos, injection_pos);
            vec4 radiance = vec4(sun.diffuse.rgb, 1.0) * vec4(voxel_albedo.rgb, 1.0);

            imageStore(brickpool_irradiances, injection_pos, radiance);

            break;
        }

        uvec3 offset_vec = uvec3(2.0 * voxel_position);
        uint offset = offset_vec.x + 2U * offset_vec.y + 4U * offset_vec.z;

        node_address = int(child_start_address + offset);
        node_position_min += vec3(child_offsets[offset]) * vec3(side_length);
        node_position_max = node_position_min + vec3(side_length);

        side_length = side_length / 2.0;
        voxel_position = 2.0 * voxel_position - vec3(offset_vec);
    }
}