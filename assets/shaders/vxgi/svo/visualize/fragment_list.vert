#version 450 core

#define VXGI_OPTIONS_BINDING 0
#define CAMERA_BINDING 1

#include vxgi/options.glsl
#include libs/camera.glsl
#include vxgi/svo/voxelize/shared.glsl

// Voxel Fragment List
layout(std430, binding = 0) buffer VoxelPositions
{
    uint voxel_positions[];
};

layout(std430, binding = 1) buffer VoxelAlbedos
{
    uint voxel_albedos[];
};

layout(location = 0) out flat uint index;

void main() {
    uint  voxel_position_uint = voxel_positions[gl_VertexID];
    uvec3 voxel_position_uvec3 = uint_to_uvec3(voxel_position_uint);
    vec3  voxel_position_vec3 = vec3(voxel_position_uvec3) / float(vxgi_options.dimension);
    index = gl_VertexID;

    gl_PointSize = 4.0;
    gl_Position = camera.projection_view_matrix * vec4(voxel_position_vec3 * 2.0 - 1.0, 1.0);
}