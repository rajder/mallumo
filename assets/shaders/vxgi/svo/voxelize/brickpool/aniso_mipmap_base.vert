#version 450 core

#include vxgi/svo/voxelize/shared.glsl

layout(location = 0) uniform uint level;

layout(std430, binding = 5) buffer NodePoolNext
{
    uint nodepool_next[];
};

layout(std430, binding = 6) buffer NodePointsPositions
{
  vec4 nodepoint_positions[];
};

layout(rgba8, binding = 0) uniform coherent volatile image3D brickpool;
layout(rgba8, binding = 1) uniform coherent volatile image3D brickpool_mip[6];

const float gaussianWeight[4] = {0.25, 0.125, 0.0625, 0.03125};

uint[8] load_child_tile(in uint tile_address) {
  uint nodes[8];

  for (int i = 0; i < 8; ++i) {
    nodes[i] = tile_address + i;
  }
  memoryBarrier();

  return nodes;
}

vec4 get_color(in uint nodes[8], in ivec3 position) {
  ivec3 child_position = ivec3(round(vec3(position) / 4.0));
  int child_index = child_position.x + 2 * child_position.y + 4 * child_position.z;

  ivec3 local_position = position - 2 * child_position;

  const uint brickpool_size = imageSize(brickpool).x / 3;
  ivec3 child_brick_address = ivec3(
      3 * (nodes[child_index] % brickpool_size),
      3 * ((nodes[child_index] / brickpool_size) % brickpool_size),
      3 * (((nodes[child_index] / brickpool_size) / brickpool_size) % brickpool_size)
  );

  return imageLoad(brickpool, child_brick_address + local_position);
}

vec4[6] mipmap_anisotropic(in uint nodes[8], in ivec3 pos) {
  vec4 col[6] = {
      vec4(0.0), vec4(0.0), vec4(0.0), vec4(0.0), vec4(0.0), vec4(0.0)
  };
  float weightSum = 0.0;
  
  // X-
  weightSum = 0.0;
  for (int x = -1; x <= 0; ++x) {
    for (int y = -1; y <= 1; ++y) {
      for (int z = -1; z <= 1; ++z) {
        const ivec3 lookupPos = pos + ivec3(x, y, z);

        if (lookupPos.x >= 0 && lookupPos.y >= 0 && lookupPos.z >= 0 &&
            lookupPos.x <= 4 && lookupPos.y <= 4 && lookupPos.z <= 4)
        {
          const int manhattanDist = abs(x) + abs(y) + abs(z);
          const float weight = gaussianWeight[manhattanDist];
          const vec4 lookupColor = get_color(nodes, lookupPos);

          col[0] += weight * lookupColor;
          weightSum += weight;
        }
      }
    }
  }
  col[0] = col[0] / weightSum;

  // X+
  weightSum = 0.0;
  for (int x = 0; x <= 1; ++x) {
    for (int y = -1; y <= 1; ++y) {
      for (int z = -1; z <= 1; ++z) {
        const ivec3 lookupPos = pos + ivec3(x, y, z);

        if (lookupPos.x >= 0 && lookupPos.y >= 0 && lookupPos.z >= 0 &&
            lookupPos.x <= 4 && lookupPos.y <= 4 && lookupPos.z <= 4)
        {
          const int manhattanDist = abs(x) + abs(y) + abs(z);
          const float weight = gaussianWeight[manhattanDist];
          const vec4 lookupColor = get_color(nodes, lookupPos);

          col[1] += weight * lookupColor;
          weightSum += weight;
        }
      }
    }
  }
  col[1] = col[1] / weightSum;

  // Y-
  weightSum = 0.0;
  for (int x = -1; x <= 1; ++x) {
    for (int y = -1; y <= 0; ++y) {
      for (int z = -1; z <= 1; ++z) {
        const ivec3 lookupPos = pos + ivec3(x, y, z);

        if (lookupPos.x >= 0 && lookupPos.y >= 0 && lookupPos.z >= 0 &&
            lookupPos.x <= 4 && lookupPos.y <= 4 && lookupPos.z <= 4)
        {
          const int manhattanDist = abs(x) + abs(y) + abs(z);
          const float weight = gaussianWeight[manhattanDist];
          const vec4 lookupColor = get_color(nodes, lookupPos);

          col[2] += weight * lookupColor;
          weightSum += weight;
        }
      }
    }
  }
  col[2] = col[2] / weightSum;

  // Y+
  weightSum = 0.0;
  for (int x = -1; x <= 1; ++x) {
    for (int y = 0; y <= 1; ++y) {
      for (int z = -1; z <= 1; ++z) {
        const ivec3 lookupPos = pos + ivec3(x, y, z);

        if (lookupPos.x >= 0 && lookupPos.y >= 0 && lookupPos.z >= 0 &&
            lookupPos.x <= 4 && lookupPos.y <= 4 && lookupPos.z <= 4)
        {
          const int manhattanDist = abs(x) + abs(y) + abs(z);
          const float weight = gaussianWeight[manhattanDist];
          const vec4 lookupColor = get_color(nodes, lookupPos);

          col[3] += weight * lookupColor;
          weightSum += weight;
        }
      }
    }
  }
  col[3] = col[3] / weightSum;

  // Z-
  weightSum = 0.0;
  for (int x = -1; x <= 1; ++x) {
    for (int y = -1; y <= 1; ++y) {
      for (int z = -1; z <= 0; ++z) {
        const ivec3 lookupPos = pos + ivec3(x, y, z);

        if (lookupPos.x >= 0 && lookupPos.y >= 0 && lookupPos.z >= 0 &&
            lookupPos.x <= 4 && lookupPos.y <= 4 && lookupPos.z <= 4)
        {
          const int manhattanDist = abs(x) + abs(y) + abs(z);
          const float weight = gaussianWeight[manhattanDist];
          const vec4 lookupColor = get_color(nodes, lookupPos);

          col[4] += weight * lookupColor;
          weightSum += weight;
        }
      }
    }
  }
  col[4] = col[4] / weightSum;

  // Z+
  weightSum = 0.0;
  for (int x = -1; x <= 1; ++x) {
    for (int y = -1; y <= 1; ++y) {
      for (int z = 0; z <= 1; ++z) {
        const ivec3 lookupPos = pos + ivec3(x, y, z);

        if (lookupPos.x >= 0 && lookupPos.y >= 0 && lookupPos.z >= 0 &&
            lookupPos.x <= 4 && lookupPos.y <= 4 && lookupPos.z <= 4)
        {
          const int manhattanDist = abs(x) + abs(y) + abs(z);
          const float weight = gaussianWeight[manhattanDist];
          const vec4 lookupColor = get_color(nodes, lookupPos);

          col[5] += weight * lookupColor;
          weightSum += weight;
        }
      }
    }
  }
  col[5] = col[5] / weightSum;

  return col;
}

void main() {
  const uint node_address = gl_VertexID;
  const int  node_level = int(nodepoint_positions[node_address].w);

  if (node_level == level) {
    const uint brickpool_size = imageSize(brickpool_mip[0]).x / 3;
    const ivec3 brick_address = ivec3(
        3 * (node_address % brickpool_size),
        3 * ((node_address / brickpool_size) % brickpool_size),
        3 * (((node_address / brickpool_size) / brickpool_size) % brickpool_size)
    );

    uint node_next = nodepool_next[node_address] & NODE_MASK_VALUE;

    if (node_next == 0) { 
      return;
    }

    uint[8] child_tiles = load_child_tile(node_next);
    
    // Center
    vec4 color[6] = mipmap_anisotropic(child_tiles, ivec3(2, 2, 2));

    // Faces
    vec4 left[6] = mipmap_anisotropic(child_tiles, ivec3(0, 2, 2));
    vec4 right[6] = mipmap_anisotropic(child_tiles, ivec3(4, 2, 2));
    vec4 bottom[6] = mipmap_anisotropic(child_tiles, ivec3(2, 0, 2));
    vec4 top[6] = mipmap_anisotropic(child_tiles, ivec3(2, 4, 2));
    vec4 near[6] = mipmap_anisotropic(child_tiles, ivec3(2, 2, 0));
    vec4 far[6] = mipmap_anisotropic(child_tiles, ivec3(2, 2, 4));

    // Edges
    vec4 nearBottom[6] = mipmap_anisotropic(child_tiles, ivec3(2, 0, 0));
    vec4 nearRight[6] = mipmap_anisotropic(child_tiles, ivec3(4, 2, 0));
    vec4 nearTop[6] = mipmap_anisotropic(child_tiles, ivec3(2, 4, 0));
    vec4 nearLeft[6] = mipmap_anisotropic(child_tiles, ivec3(0, 2, 0));
    vec4 farBottom[6] = mipmap_anisotropic(child_tiles, ivec3(2, 0, 4));
    vec4 farRight[6] = mipmap_anisotropic(child_tiles, ivec3(4, 2, 4));
    vec4 farTop[6] = mipmap_anisotropic(child_tiles, ivec3(2, 4, 4));
    vec4 farLeft[6] = mipmap_anisotropic(child_tiles, ivec3(0, 2, 4));
    vec4 leftBottom[6] = mipmap_anisotropic(child_tiles, ivec3(0, 0, 2));
    vec4 leftTop[6] = mipmap_anisotropic(child_tiles, ivec3(0, 4, 2));
    vec4 rightBottom[6] = mipmap_anisotropic(child_tiles, ivec3(4, 0, 2));
    vec4 rightTop[6] = mipmap_anisotropic(child_tiles, ivec3(4, 4, 2));

    // corners
    vec4 nearRightTop[6] = mipmap_anisotropic(child_tiles, ivec3(4, 4, 0));
    vec4 nearRightBottom[6] = mipmap_anisotropic(child_tiles, ivec3(4, 0, 0));
    vec4 nearLeftTop[6] = mipmap_anisotropic(child_tiles, ivec3(0, 4, 0));
    vec4 nearLeftBottom[6] = mipmap_anisotropic(child_tiles, ivec3(0, 0, 0));
    vec4 farRightTop[6] = mipmap_anisotropic(child_tiles, ivec3(4, 4, 4));
    vec4 farRightBottom[6] = mipmap_anisotropic(child_tiles, ivec3(4, 0, 4));
    vec4 farLeftTop[6] = mipmap_anisotropic(child_tiles, ivec3(0, 4, 4));
    vec4 farLeftBottom[6] = mipmap_anisotropic(child_tiles, ivec3(0, 0, 4));

    for(int i = 0; i < 6; i++) {
        imageStore(brickpool_mip[i], brick_address + ivec3(1,1,1), color[i]);

        imageStore(brickpool_mip[i], brick_address + ivec3(0,1,1), left[i]);
        imageStore(brickpool_mip[i], brick_address + ivec3(2,1,1), right[i]);
        imageStore(brickpool_mip[i], brick_address + ivec3(1,0,1), bottom[i]);
        imageStore(brickpool_mip[i], brick_address + ivec3(1,2,1), top[i]);
        imageStore(brickpool_mip[i], brick_address + ivec3(1,1,0), near[i]);
        imageStore(brickpool_mip[i], brick_address + ivec3(1,1,2), far[i]);

        imageStore(brickpool_mip[i], brick_address + ivec3(1,0,0), nearBottom[i]);
        imageStore(brickpool_mip[i], brick_address + ivec3(2,1,0), nearRight[i]);
        imageStore(brickpool_mip[i], brick_address + ivec3(1,2,0), nearTop[i]);
        imageStore(brickpool_mip[i], brick_address + ivec3(0,1,0), nearLeft[i]);
        imageStore(brickpool_mip[i], brick_address + ivec3(1,0,2), farBottom[i]);
        imageStore(brickpool_mip[i], brick_address + ivec3(2,1,2), farRight[i]);
        imageStore(brickpool_mip[i], brick_address + ivec3(1,2,2), farTop[i]);
        imageStore(brickpool_mip[i], brick_address + ivec3(0,1,2), farLeft[i]);
        imageStore(brickpool_mip[i], brick_address + ivec3(0, 0, 1), leftBottom[i]);
        imageStore(brickpool_mip[i], brick_address + ivec3(2, 0, 1), rightBottom[i]);
        imageStore(brickpool_mip[i], brick_address + ivec3(0, 2, 1), leftTop[i]);
        imageStore(brickpool_mip[i], brick_address + ivec3(2, 2, 1), rightTop[i]);

        imageStore(brickpool_mip[i], brick_address + ivec3(2, 2, 0), nearRightTop[i]);
        imageStore(brickpool_mip[i], brick_address + ivec3(2, 0, 0), nearRightBottom[i]);
        imageStore(brickpool_mip[i], brick_address + ivec3(0, 2, 0), nearLeftTop[i]);
        imageStore(brickpool_mip[i], brick_address + ivec3(0, 0, 0), nearLeftBottom[i]);
        imageStore(brickpool_mip[i], brick_address + ivec3(2, 2, 2), farRightTop[i]);
        imageStore(brickpool_mip[i], brick_address + ivec3(2, 0, 2), farRightBottom[i]);
        imageStore(brickpool_mip[i], brick_address + ivec3(0, 2, 2), farLeftTop[i]);
        imageStore(brickpool_mip[i], brick_address + ivec3(0, 0, 2), farLeftBottom[i]);
    }
  }
}
