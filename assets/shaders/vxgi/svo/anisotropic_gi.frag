#version 450 core

#define VXGI_OPTIONS_BINDING 0
#define CAMERA_BINDING 1
#define SUN_BINDING 2
#define SUN_SHADOWMAP_BINDING 5

#include libs/consts.glsl
#include libs/cook_torrance_fresnel.glsl
#include libs/cook_torrance_distribution.glsl
#include libs/cook_torrance_geometry.glsl
#include libs/pbr.glsl
#include libs/camera.glsl
#include sun/sun.glsl
#include vxgi/options.glsl
#include vxgi/svo/voxelize/shared.glsl

// Nodepool
layout(std430, binding = 0) buffer NodePoolNext { uint nodepool_next[]; };

// Brickpool
layout(binding = 6) uniform sampler3D brickpool_irradiances_base;
layout(binding = 7) uniform sampler3D brickpool_irradiances_mips[6];

void correct_alpha(inout vec4 color, in float alpha_correction) {
    float color_old = color.a;
    color.a = 1.0 - pow(1.0 - color.a, alpha_correction);
    color.xyz = color.xyz * (color.a / clamp(color_old, 0.0001, 10000.0));
}

ivec3 brick_address_base(int node_address)
{
    const uint brickpool_size = textureSize(brickpool_irradiances_base, 0).x / 3;

    return ivec3(
        3 * (node_address % brickpool_size),
        3 * ((node_address / brickpool_size) % brickpool_size),
        3 * (node_address / (brickpool_size * brickpool_size))
    );
}

ivec3 brick_address_mips(int node_address)
{
    const uint brickpool_size = textureSize(brickpool_irradiances_mips[0], 0).x / 3;

    return ivec3(
        3 * (node_address % brickpool_size),
        3 * ((node_address / brickpool_size) % brickpool_size),
        3 * (node_address / (brickpool_size * brickpool_size))
    );
}

int traverse_octree_level(in vec3 position, 
                          in uint target_level,
                          inout vec3 node_position_min,
                          inout vec3 node_position_max,
                          inout int parent_address,
                          inout vec3 parent_min,
                          inout vec3 parent_max)
{  
    // Clear the out-parameters
    node_position_min = vec3(0.0);
    node_position_max = vec3(1.0);

    parent_address = 0;
    parent_min = vec3(0.0);
    parent_max = vec3(1.0);
  
    int node_address = 0;
    float side_length = 1.0;

    position = clamp(position, vec3(0.0, 0.0, 0.0), vec3(1.0, 1.0, 1.0));
    
    for (uint level = 0; level < target_level; ++level) {
        uint node = nodepool_next[node_address] & NODE_MASK_VALUE;

        // if current node points to *null* (0) nodepool
        uint child_start_address = node & NODE_MASK_VALUE;
        if (child_start_address == 0U) {
            node_address = int(NODE_NOT_FOUND);
            break;
        }
    
        uvec3 offset_vec = uvec3(2.0 * position);
        uint offset = offset_vec.x + 2U * offset_vec.y + 4U * offset_vec.z;

        side_length = side_length / 2.0; 

        parent_address = node_address;
        node_address = int(child_start_address + offset);

        parent_min = node_position_min;
        parent_max = node_position_max;

        node_position_min += vec3(child_offsets[offset]) * vec3(side_length);
        node_position_max = node_position_min + vec3(side_length);
       
        position = 2.0 * position - vec3(offset_vec);
    }

    return node_address;
}

vec4 vxgi_sample_function(vec3 position, vec3 weight, uvec3 face, float lod, VoxelTextureType type) {
  face.x = clamp(face.x, 0U, 1U);
  face.y = clamp(face.y, 2U, 3U);
  face.z = clamp(face.z, 4U, 5U);

  float t_enter = 0.0;
  float t_leave = 0.0;

  vec3 min_child_node = vec3(0.0);
  vec3 max_child_node = vec3(1.0);
  vec3 min_parent_node = vec3(0.0);
  vec3 max_parent_node = vec3(1.0);

  const float brickpool_size = float(textureSize(brickpool_irradiances_base, 0).x);
  const float voxel_step = 1.0 / brickpool_size;

  lod = clamp(lod, 0.0f, float(vxgi_options.levels) - 1.00001);
  float sample_lod = (float(vxgi_options.levels) - 1.0) - lod;

  // Readjust mipmap level(+it's associated size) based on actual available sizes of mipmaps
  const uint child_level = uint(ceil(sample_lod));
  const float node_size = node_sizes[child_level];

  int parent_address;
  int child_address = traverse_octree_level(position, child_level, 
                                          min_child_node, max_child_node, 
                                          parent_address, min_parent_node, max_parent_node);

  if (child_address == int(NODE_NOT_FOUND)) {
      return vec4(0.0);
  }

  // Find bricks coressponding to child and parent nodes
  ivec3 child_brick_address;
  if (child_level == vxgi_options.levels - 1) {
    child_brick_address = brick_address_base(child_address);
  } else {
    child_brick_address = brick_address_mips(child_address);
  }
  ivec3 parent_brick_address = brick_address_mips(parent_address);

  // Texture position of brick
  const vec3 child_brick_enter = (vec3(child_brick_address) + 0.5) / brickpool_size;
  const vec3 parent_brick_enter = (vec3(parent_brick_address) + 0.5) / brickpool_size;

  // Local position inside the child and parent node in texture space
  const vec3 child_node_position = (position - min_child_node) / node_size;
  const vec3 parent_node_position = (position - min_parent_node) / (node_size * 2.0);

  // Calculate global texture position in brickpool
  const vec3 child_interpolate_position = child_brick_enter + child_node_position * (2.0 * voxel_step);
  const vec3 parent_interpolate_position = parent_brick_enter + parent_node_position * (2.0 * voxel_step);

  // Retrieve trilineraly interpolated values from child and parent bricks
  vec4 child_color;
  if (child_level == vxgi_options.levels - 1) { 
    child_color = texture(brickpool_irradiances_base, child_interpolate_position).rgba;
  } else {
    child_color = weight.x * texture(brickpool_irradiances_mips[face.x], child_interpolate_position).rgba
                + weight.y * texture(brickpool_irradiances_mips[face.y], child_interpolate_position).rgba
                + weight.z * texture(brickpool_irradiances_mips[face.z], child_interpolate_position).rgba;
  }

  vec4 parent_color = weight.x * texture(brickpool_irradiances_mips[face.x], parent_interpolate_position).rgba
                    + weight.y * texture(brickpool_irradiances_mips[face.y], parent_interpolate_position).rgba
                    + weight.z * texture(brickpool_irradiances_mips[face.z], parent_interpolate_position).rgba;


  // Alpha correct
  const float alpha_correction = float(pow2[child_level + 1]) / float(pow2[vxgi_options.levels]);

  correct_alpha(child_color, alpha_correction);
  correct_alpha(parent_color, alpha_correction * 2);

  return mix(parent_color, child_color, fract(sample_lod));
}

#include vxgi/gi_shared.glsl