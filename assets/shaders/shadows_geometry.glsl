#version 450 core

layout (triangles) in;
layout (triangle_strip, max_vertices=18) out;

layout(std140, binding = 0) uniform Globals
{
    mat4 shadow_projections[6];
    vec4 pos_far;
} globals;

in gl_PerVertex
{
  vec4 gl_Position;
} gl_in[];

in vertexOut
{
    flat uint casts_shadows;
} vertex_out[];

out gl_PerVertex
{
  vec4 gl_Position;
};

out geometryOut {
  vec3 frag_pos;
  flat uint casts_shadows;
} geometry_out;

void main()
{
    for(int face = 0; face < 6; ++face)
    {
        gl_Layer = face;
        for(int i = 0; i < 3; ++i)
        {
            geometry_out.frag_pos = gl_in[i].gl_Position.xyz;
            geometry_out.casts_shadows = vertex_out[i].casts_shadows;
            gl_Position = globals.shadow_projections[face] * gl_in[i].gl_Position;
            EmitVertex();
        }    
        EndPrimitive();
    }
}  