#version 450

struct Vertex {
    vec4 color;
    vec2 position;
    vec2 uv;
    uint mode;
};

layout(binding = 0, std430) buffer Vertices {
    Vertex vertices[];
};

out VertexData {
    vec4 color;
    vec2 uv;
    flat uint mode;
} vertex_out;

void main() {
    Vertex vertex = vertices[gl_VertexID];

    vertex_out.color = vertex.color;
    vertex_out.uv = vertex.uv;
    vertex_out.mode = vertex.mode;

    gl_Position = vec4(vertex.position, 0.0, 1.0);
}